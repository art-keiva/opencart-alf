<?php
// Heading
$_['heading_title']         = 'Прайсы';

// Tab prices
$_['tab_price']             = 'Прайс';

// Text
$_['text_success']         = 'Настройки успешно изменены!';
$_['text_edit']            = 'Редактирование';
$_['text_error']           = 'Ошибки';

// Entry
$_['entry_price_name']              = 'Название';
$_['entry_price_description']       = 'Описание';
$_['entry_price_link']              = 'Ссылка';
$_['entry_price_image']             = 'Изображение';
$_['entry_price_sort']              = 'Сортировка';

// Error
$_['error_warning']                 = 'Ошибка! Проверьте форму на наличие ошибок!';
$_['error_permission']              = 'У Вас нет прав для изменения настроек!';

// Fixtures
$_['fixture_1_name']                = 'Прайс 1';
$_['fixture_1_description']         = 'Разнообразный и богатый опыт реализация 1';
$_['fixture_2_name']                = 'Прайс 2';
$_['fixture_2_description']         = 'Разнообразный и богатый опыт реализация 2';