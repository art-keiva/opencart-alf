<?php
// Heading
$_['heading_title']          = 'Баннеры 6';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_list']              = 'Список записей';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';

// Column
$_['column_name']            = 'Название';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название';
$_['entry_link']             = 'Ссылка';
$_['entry_description']      = 'Описание';
$_['entry_image']            = 'Изображение (208х200)';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_status']           = 'Статус';

// Help

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения записей!';
$_['error_name']             = 'Название должно быть от 2 до 255 символов!';


