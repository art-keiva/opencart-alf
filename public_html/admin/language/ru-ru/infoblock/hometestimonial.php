<?php
// Heading
$_['heading_title']          = 'Отзывы на главной';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_list']              = 'Отзывы';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';
$_['text_default']           = 'Основной магазин';

// Column
$_['column_name']            = 'Название';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название';
$_['entry_job']              = 'Должность';
$_['entry_description']      = 'Описание';
$_['entry_meta_title'] 	     = 'Мета-тег Title';
$_['entry_meta_keyword']     = 'Мета-тег Keywords';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_parent']           = 'Родительская рубрика';
$_['entry_filter']           = 'Фильтры';
$_['entry_store']            = 'Магазины';
$_['entry_image']            = 'Изображение отзыва (92x92)';
$_['entry_top']              = 'Главное меню';
$_['entry_column']           = 'Столбцы';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_status']           = 'Статус';
$_['entry_layout']           = 'Макет';

// Help
$_['help_filter']            = '(Автозаполнение)';
$_['help_keyword']           = 'Должно быть уникальным на всю систему.';
$_['help_top']               = 'Показывать в главном меню (только для главных родительских категорий).';
$_['help_column']            = 'Количество столбцов в выпадающем меню рубрики (только для главных родительских категорий)';

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения категорий!';
$_['error_name']             = 'Название рубрики должно быть от 2 до 255 символов!';
$_['error_meta_title']       = 'Ключевое слово должно быть от 3 до 255 символов!';
$_['error_keyword']          = 'SEO URL занят!';
$_['error_parent']           = 'Родительская рубрика выбрана неправильно!';


