<?php
// Heading
$_['heading_title']         = 'Инфоблоки';

// Tags
$_['tag_homeblocks']        = 'Блоки на главной';
$_['tag_seo']               = 'SEO страниц';
$_['tag_contact']           = 'Контактная информация';
$_['tag_social']            = 'Социальные сети';

// Tab infoblocks
$_['tab_info1']             = 'Блок 1';
$_['tab_info2']             = 'Блок 2';
$_['tab_info3']             = 'Блок 3';
$_['tab_info4']             = 'Блок 4';
$_['tab_info5']             = 'Блок 5';
// Tab home page announce
$_['tab_slider']            = 'Слайдер';
$_['tab_advantage']         = 'Преимущества';
$_['tab_banner1']           = 'Банер 1';
$_['tab_banner2']           = 'Банер 2';
$_['tab_banner3']           = 'Банер 3';
$_['tab_banner4']           = 'Банер 4';
$_['tab_banner5']           = 'Банер 5';
$_['tab_banner6']           = 'Банер 6';
$_['tab_faq']               = 'Вопросы ответы';
$_['tab_testimonial']       = 'Отзывы';
$_['tab_action']            = 'Акции';
$_['tab_news']              = 'Новости';
$_['tab_service']           = 'Услуги';
$_['tab_featured']          = 'Рекомендуемые';
$_['tab_special']           = 'Избранные';
$_['tab_portfolio']         = 'Портфолио';
$_['tab_specialist']        = 'Специалисты';
// Tab seo
$_['tab_seohome']           = 'Главная';
$_['tab_seocatalog']        = 'Каталог';
$_['tab_seoaction']         = 'Акции';
$_['tab_seonews']           = 'Новости';
$_['tab_seoservice']        = 'Услуги';
$_['tab_seocontact']        = 'Контакты';
$_['tab_seofaq']            = 'Вопросы и ответы';
$_['tab_seoportfolio']      = 'Портфолио';
$_['tab_seospecialist']     = 'Специалисты';
$_['tab_seoprice']          = 'Прайсы';
// Tab contact
$_['tab_contact_address']           = 'Адрес';
$_['tab_contact_telephone1']        = 'Телефон 1';
$_['tab_contact_telephone2']        = 'Телефон 2';
$_['tab_contact_telephone3']        = 'Телефон 3';
$_['tab_contact_email']             = 'Email';
$_['tab_contact_emailrecipient']    = 'Email для уведомлений';
$_['tab_contact_comment']           = 'Слоган компании';
$_['tab_contact_open']              = 'Режим работы';
$_['tab_contact_coords']            = 'Координаты на карте';
$_['tab_contact_geocode']           = 'Виджет карты';
$_['tab_contact_imagemap']          = 'Изображение компании';
// Tab social
$_['tab_social_vk']         = 'Вконтакте';
$_['tab_social_insta']      = 'Инстаграм';

$_['tab_infoblock_1']      = 'Блок 1';
$_['tab_infoblock_2']      = 'Блок 2';
$_['tab_infoblock_3']      = 'Блок 3';
$_['tab_infoblock_4']      = 'Блок 4';
$_['tab_infoblock_5']      = 'Блок 5';
$_['tab_infoblock_6']      = 'Блок 6';
$_['tab_infoblock_7']      = 'Блок 7';
$_['tab_infoblock_8']      = 'Блок 8';
$_['tab_infoblock_9']      = 'Блок 9';

// Text
$_['text_success']         = 'Настройки успешно изменены!';
$_['text_edit']            = 'Редактирование';
$_['text_error']           = 'Ошибки';

// Entry
$_['entry_infoblock_name']              = 'Название';
$_['entry_infoblock_description']       = 'Описание';
$_['entry_infoblock_link']              = 'Ссылка';
$_['entry_infoblock_image']             = 'Изображение';
$_['entry_infoblock_seo_title']         = 'Meta Title';
$_['entry_infoblock_seo_description']   = 'Meta Description';
$_['entry_infoblock_seo_keyword']       = 'Meta Keywords';

// Error
$_['error_warning']                 = 'Ошибка! Проверьте форму на наличие ошибок!';
$_['error_permission']              = 'У Вас нет прав для изменения настроек!';

// Fixtures
$_['fixture_slider_name']           = $_['tab_slider'];
$_['fixture_advantage_name']        = $_['tab_advantage'];
$_['fixture_banner1_name']          = $_['tab_banner1'];
$_['fixture_banner2_name']          = $_['tab_banner2'];
$_['fixture_banner3_name']          = $_['tab_banner3'];
$_['fixture_banner4_name']          = $_['tab_banner4'];
$_['fixture_banner5_name']          = $_['tab_banner5'];
$_['fixture_banner6_name']          = $_['tab_banner6'];
$_['fixture_faq_name']              = $_['tab_faq'];
$_['fixture_testimonial_name']      = $_['tab_testimonial'];
$_['fixture_action_name']           = $_['tab_action'];
$_['fixture_news_name']             = $_['tab_news'];
$_['fixture_service_name']          = $_['tab_service'];
$_['fixture_featured_name']         = $_['tab_featured'];
$_['fixture_special_name']          = $_['tab_special'];
$_['fixture_portfolio_name']        = $_['tab_portfolio'];
$_['fixture_specialist_name']       = $_['tab_specialist'];
$_['fixture_seohome_title']         = $_['tab_seohome'];
$_['fixture_seocatalog_title']      = $_['tab_seocatalog'];
$_['fixture_seoaction_title']       = $_['tab_seoaction'];
$_['fixture_seonews_title']         = $_['tab_seonews'];
$_['fixture_seoservice_title']      = $_['tab_seoservice'];
$_['fixture_seocontact_title']      = $_['tab_seocontact'];
$_['fixture_seofaq_title']          = $_['tab_seofaq'];
$_['fixture_seoportfolio_title']    = $_['tab_seoportfolio'];
$_['fixture_seospecialist_title']   = $_['tab_seospecialist'];
$_['fixture_seoprice_title']        = $_['tab_seoprice'];

$_['fixture_service_description']       = 'Разнообразный и богатый опыт реализация';
$_['fixture_service_image']             = 'catalog/demo/common/blur-crowd-indoors-950902.jpg';
$_['fixture_service_link']              = 'https://ya.ru/';
$_['fixture_social_vk']                 = 'http://vk.com/';
$_['fixture_social_insta']              = 'http://www.instagram.com/';
$_['fixture_contact_address']           = 'Адрес улица';
$_['fixture_contact_telephone1']        = '8-800-700';
$_['fixture_contact_telephone2']        = '8-800-800';
$_['fixture_contact_telephone3']        = '8-800-900';
$_['fixture_contact_email']             = '1@ya.ru';
$_['fixture_contact_emailrecipient']    = 'init320@gmail.com';
$_['fixture_contact_comment']           = 'Разнообразный и богатый опыт реализация';
$_['fixture_contact_open']              = 'Пн-Пт: 9:00 - 17:00';
$_['fixture_contact_coords']            = '55.669295, 52.341580';
$_['fixture_contact_geocode']           = '<a class="dg-widget-link" href="http://2gis.ru/moscow/firm/70000001019868271/center/37.664458751678474,55.75887693887913/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Москвы</a><div class="dg-widget-link"><a href="http://2gis.ru/moscow/center/37.664643,55.758915/zoom/16/routeTab/rsType/bus/to/37.664643,55.758915╎Transitplus, компания по доставке грузов из Китая?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Transitplus, компания по доставке грузов из Китая</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":\'100%\',"height":500,"borderColor":"rgba(0, 0, 0, 0.05)","pos":{"lat":55.75887693887913,"lon":37.664458751678474,"zoom":16},"opt":{"city":"moscow"},"org":[{"id":"70000001019868271"}]});</script><noscript style="color:#dadada;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>';
$_['fixture_contact_imagemap']          = 'catalog/demo/common/map-moscow.jpg';
$_['fixture_info1_name']                = 'Рекламный блок';
$_['fixture_info1_description']         = 'Разнообразный и богатый опыт реализация';
$_['fixture_info1_image']               = 'catalog/demo/common/blur-crowd-indoors-950902.jpg';
$_['fixture_info1_link']                = 'https://ya.ru/';