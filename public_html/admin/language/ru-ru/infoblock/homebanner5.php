<?php
// Heading
$_['heading_title']          = 'Баннеры 5';

// Text
$_['text_success']           = 'Настройки успешно изменены!';
$_['text_list']              = 'Список записей';
$_['text_add']               = 'Добавить';
$_['text_edit']              = 'Редактирование';

// Column
$_['column_name']            = 'Название';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название';
$_['entry_link']             = 'Ссылка';
$_['entry_description']      = 'Описание';
$_['entry_image']            = 'Изображение (208х200)';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_status']           = 'Статус';

$_['entry_manipulator']            = 'Наличие манипулятора';
$_['entry_weight']            = 'Грузоподъемность, кг';
$_['entry_length']            = 'Кузов длина, м';
$_['entry_width']            = 'Кузов ширина, м';
$_['entry_indent']            = 'Требуемый погрузочный зазор между упаковками, м';
$_['entry_fuel_need']            = 'Расход топлива, л/км';
$_['entry_fuel_cost']            = 'Стоимость топлива, руб/л';
$_['entry_amortization']            = 'Амортизация а/м, руб/км';
$_['entry_interest_seller']            = 'Интересы владельца, руб/км';
$_['entry_tariff_driver']            = 'Тариф водителю по перевозке груза, руб/км';
$_['entry_tariff_landing']            = 'Тариф водит по выгрузке, руб/транс.упак.(поддон)';

// Help

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения записей!';
$_['error_name']             = 'Название должно быть от 2 до 255 символов!';


