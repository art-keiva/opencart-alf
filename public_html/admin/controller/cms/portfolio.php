<?php
class ControllerCmsPortfolio extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('cms/portfolio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('cms/portfolio');

        $this->getList();
    }

    public function add() {
        $this->load->language('cms/portfolio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('cms/portfolio');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_cms_portfolio->addPortfolio($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('cms/portfolio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('cms/portfolio');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_cms_portfolio->editPortfolio($this->request->get['portfolio_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('cms/portfolio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('cms/portfolio');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $portfolio_id) {
                $this->model_cms_portfolio->deletePortfolio($portfolio_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'title';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('cms/portfolio/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('cms/portfolio/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['portfolios'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $portfolio_total = $this->model_cms_portfolio->getTotalPortfolio();

        $results = $this->model_cms_portfolio->getPortfolios($filter_data);

        foreach ($results as $result) {
            $data['portfolios'][] = array(
                'portfolio_id'   => $result['portfolio_id'],
                'title'          => $result['title'],
                'sort_order'     => $result['sort_order'],
                'edit'           => $this->url->link('cms/portfolio/edit', 'user_token=' . $this->session->data['user_token'] . '&portfolio_id=' . $result['portfolio_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_title'] = $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . '&sort=title' . $url, true);
        $data['sort_sort_order'] = $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $portfolio_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($portfolio_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($portfolio_total - $this->config->get('config_limit_admin'))) ? $portfolio_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $portfolio_total, ceil($portfolio_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('cms/portfolio_list', $data));
    }

    protected function getForm() {
        $data['text_form'] = !isset($this->request->get['portfolio_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['portfolio_id'])) {
            $data['action'] = $this->url->link('cms/portfolio/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('cms/portfolio/edit', 'user_token=' . $this->session->data['user_token'] . '&portfolio_id=' . $this->request->get['portfolio_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('cms/portfolio', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['portfolio_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $portfolio_info = $this->model_cms_portfolio->getPortfolio($this->request->get['portfolio_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        $this->load->model('tool/image');

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        // Images
        if (isset($this->request->post['portfolio_image'])) {
            $portfolio_images = $this->request->post['portfolio_image'];
        } elseif (isset($this->request->get['portfolio_id'])) {
            $portfolio_images = $this->model_cms_portfolio->getPortfolioImages($this->request->get['portfolio_id']);
        } else {
            $portfolio_images = array();
        }

        $data['portfolio_images'] = array();

        foreach ($portfolio_images as $portfolio_image) {
            if (is_file(DIR_IMAGE . $portfolio_image['image'])) {
                $image = $portfolio_image['image'];
                $thumb = $portfolio_image['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['portfolio_images'][] = array(
                'title'      => $portfolio_image['title'],
                'special'    => $portfolio_image['special'],
                'image'      => $image,
                'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $portfolio_image['sort_order']
            );
        }

        if (isset($this->request->post['title'])) {
            $data['title'] = $this->request->post['title'];
        } elseif (!empty($portfolio_info)) {
            $data['title'] = $portfolio_info['title'];
        } else {
            $data['title'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($portfolio_info)) {
            $data['status'] = $portfolio_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($portfolio_info)) {
            $data['sort_order'] = $portfolio_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('cms/portfolio_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'cms/portfolio')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['portfolio_description'] as $language_id => $value) {
            if ((utf8_strlen($value['title']) < 1) || (utf8_strlen($value['title']) > 64)) {
                $this->error['title'][$language_id] = $this->language->get('error_title');
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'cms/portfolio')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}