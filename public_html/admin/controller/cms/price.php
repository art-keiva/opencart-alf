<?php
class ControllerCmsPrice extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('cms/price');

        $this->load->model('cms/price');

        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

        $results = $this->model_cms_price->getPrice();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_cms_price->editPrice($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('cms/price', 'user_token=' . $this->session->data['user_token'], true));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');

        $data['button_save'] = $this->language->get('button_save');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('cms/price', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['action'] = $this->url->link('cms/price', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];

        $data['tags'] = $this->getTags();

        $data['priceTags'] = $this->getPriceData(['tagName' => 'price', 'results' => $results]);

        $data['hideClass'] = 'hide';
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('cms/price', $data));
    }

    private function getPriceData(array $data = [])
    {
        if (!isset($data['tagName']) && !isset($data['results'])) {
            throw new \Exception('Too few arguments');
        }

        $allTags = $this->getTags();
        $tags = $allTags[$data['tagName']];

        // INSERT NEW TAGS IF THEY NOT EXISTS
        $this->insertTags(['tags' => $tags['tags'], 'keyVars' => $tags['keys']]);

        $tags = $this->tagsHelper([
            'tags' => $tags['tags'],
            'keyVars' => $tags['keys'],
            'post' => $this->request->post,
            'results' => $data['results'],
        ]);

        return $tags;
    }

    private function insertTags($data = []) {
        if (empty($data)) {
            return;
        }

        $redirect = false;

        foreach ($data['tags'] as $tag => $key) {
            foreach ($data['keyVars'] as $keyword) {
                $value = '';
                if (isset($this->language->data['fixture_' . $tag . '_' . $keyword])) {
                    $value = $this->language->data['fixture_' . $tag . '_' . $keyword];
                }

                // insert zero value for all rows
                if ($keyword == 'sort') {
                    $value = 0;
                }

                $post = [
                    'key' => 'price_' . $tag . '_' . $keyword,
                    'value' => $value,
                ];

                $priceResults = $this->model_cms_price->getPrice(['key' => 'price_' . $tag . '_' . $keyword]);

                if (!count($priceResults)) {
                    $redirect = true;
                    $this->model_cms_price->addPrice($post);
                }
            }
        }

        if ($redirect) {
            $this->response->redirect($this->url->link('cms/price', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    private function tagsHelper($data = []) {
        if (empty($data)) {
            return;
        }

        $pricetags = [];
        $results = $data['results'];

        foreach ($data['tags'] as $value => $key) {
            foreach ($data['keyVars'] as $keyword) {
                $infoTag = 'price_' . $value . '_' . $keyword;

                if (isset($data['post'][$infoTag])) {
                    $tagOutput = $data['post'][$infoTag];
                } else if (isset($results[$infoTag])) {
                    $tagOutput = $results[$infoTag];
                } else {
                    $tagOutput = '';
                }

                if ($keyword == 'sort') {
                    if (empty($tagOutput)) {
                        $tagOutput = 0;
                    }
                }

                $pricetags[$value][$keyword] = $tagOutput;

            }
        }

        return $pricetags;
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'cms/price')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post as $key => $value) {
            $parts = explode('_', $key);

            if (isset($parts[2]) && $parts[2] == 'sort') {
                if (empty($value)) {
                    $this->request->post[$key] = 0;
                }
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    private function getTags() {
        $tags = [
            'price' => [
                'tags' => [],
                'keys' => [
                    'name',
                    'sort',
                    'description',
                ],
            ],
        ];

        $priceResults = $this->model_cms_price->getPrice();

        foreach ($priceResults as $key => $value) {
            $parts = explode('_', $key);

            if (isset($parts[1]) && !isset($tags['price']['tags'][$parts[1]])) {
                $tags['price']['tags'][$parts[1]] = [
                    'name' => 1,
                    'sort' => 0,
                    'description' => 1,
                ];
            }
        }

        return $tags;
    }

}
