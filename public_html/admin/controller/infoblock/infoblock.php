<?php
class ControllerInfoblockInfoblock extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('infoblock/infoblock');

		$this->load->model('infoblock/infoblock');

        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

		$results = $this->model_infoblock_infoblock->getInfoblock();

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_infoblock_infoblock->editInfoblock($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('infoblock/infoblock', 'user_token=' . $this->session->data['user_token'], true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');

		$data['button_save'] = $this->language->get('button_save');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('infoblock/infoblock', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('infoblock/infoblock', 'user_token=' . $this->session->data['user_token'], true);

		$data['user_token'] = $this->session->data['user_token'];

        $data['tags'] = $this->getTags();

        $data['infoTags'] = $this->getTagData(['tagName' => 'info', 'results' => $results]);
        $data['homeblockTags'] = $this->getTagData(['tagName' => 'homeblock', 'results' => $results]);
        $data['seoTags'] = $this->getTagData(['tagName' => 'seo', 'results' => $results]);
        $data['contactTags'] = $this->getTagData(['tagName' => 'contact', 'results' => $results]);
        $data['socialTags'] = $this->getTagData(['tagName' => 'social', 'results' => $results]);

        $data['hideClass'] = 'hide';
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('infoblock/infoblock', $data));
	}

    public function setting() {
        $settings = [];

        //foreach ($this->getTags() as $tags) {
        //    foreach ($tags as $key => $tagsAndKeys) {
        //        foreach ($tagsAndKeys as $value) {
        //            if ($key === 'tags') {
        //
        //            }
        //            var_dump($key, $value);
        //        }
        //    }
        //}

        $settings = $this->getTags();

        return $settings;
    }

    private function getTagData(array $data = [])
    {
        if (!isset($data['tagName']) && !isset($data['results'])) {
            throw new \Exception('Too few arguments');
        }

        $allTags = $this->getTags();
        $tags = $allTags[$data['tagName']];

        // INSERT NEW TAGS IF THEY NOT EXISTS
        $this->insertTags(['tags' => $tags['tags'], 'keyVars' => $tags['keys']]);

        $tags = $this->tagsHelper([
            'tags' => $tags['tags'],
            'keyVars' => $tags['keys'],
            'post' => $this->request->post,
            'results' => $data['results'],
        ]);

        return $tags;
    }

	private function insertTags($data = []) {
        if (empty($data)) {
            return;
        }

        $redirect = false;

        foreach ($data['tags'] as $tag => $key) {
            foreach ($data['keyVars'] as $keyword) {
                $value = '';
                if (isset($this->language->data['fixture_' . $tag . '_' . $keyword])) {
                    $value = $this->language->data['fixture_' . $tag . '_' . $keyword];
                }

                $post = [
                    'key' => 'infoblock_' . $tag . '_' . $keyword,
                    'value' => $value,
                ];

                $infoblockResults = $this->model_infoblock_infoblock->getInfoblock(['key' => 'infoblock_' . $tag . '_' . $keyword]);

                if (!count($infoblockResults)) {
                    $redirect = true;
                    $this->model_infoblock_infoblock->addInfoblock($post);
                }
            }
        }

        if ($redirect) {
            $this->response->redirect($this->url->link('infoblock/infoblock', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

	private function tagsHelper($data = []) {
        if (empty($data)) {
            return;
        }

        $homeblocktags = [];
        $stucked = [];
        $results = $data['results'];
        $allTags = $this->getTags();

        // tags linked in one line
        foreach ($allTags as $key => $allTag) {
            if (isset($allTag['oneline']) && $allTag['oneline'] === 1 && !in_array($key, $stucked)) {
                $stucked[] = $key;
            }
        }

        foreach ($data['tags'] as $value => $key) {
            foreach ($data['keyVars'] as $keyword) {
                $infoTag = 'infoblock_' . $value . '_' . $keyword;
                $thumb = $this->model_tool_image->resize('no_image.png', 100, 100);

                if (isset($data['post'][$infoTag])) {
                    $tagOutput = $data['post'][$infoTag];
                } else if (isset($results[$infoTag])) {
                    $tagOutput = $results[$infoTag];
                } else {
                    $tagOutput = '';
                }

                $keywordImage = substr($keyword, 0, 5);

                if ($keyword === 'image' || $keywordImage === 'image') {
                    if (isset($data['post'][$infoTag]) && is_file(DIR_IMAGE . $data['post'][$infoTag])) {
                        $thumb = $this->model_tool_image->resize($data['post'][$infoTag], 100, 100);
                    } elseif (isset($results[$infoTag]) && !empty($results[$infoTag]) && is_file(DIR_IMAGE . $results[$infoTag])) {
                        $thumb = $this->model_tool_image->resize($results[$infoTag], 100, 100);
                    }

                    if (in_array($value, $stucked)) {
                        $homeblocktags[$infoTag]['thumb'] = $thumb;
                        $homeblocktags[$infoTag]['type'] = 'image';
                    } else {
                        $homeblocktags[$value]['thumb'] = $thumb;
                    }
                }

                if (in_array($value, $stucked)) {
                    $homeblocktags[$infoTag]['trans'] = $this->language->get('tab_' . $value . '_' . $keyword);
                    $homeblocktags[$infoTag]['value'] = $tagOutput;
                    continue;
                }

                $homeblocktags[$value][$keyword] = $tagOutput;

            }

            if (in_array($value, $stucked)) {
                continue;
            }

            $homeblocktags[$value]['trans'] = $this->language->get('tab_' . $value);
        }

        return $homeblocktags;
    }

    private function getTags() {
        $tags = [
            'info' => [
                'tags' => [
                    'info1' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'info2' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'info3' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'info4' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'info5' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                ],
                'keys' => [
                    'name',
                    'description',
                    'image',
                    'link',
                ],
            ],
            'homeblock' => [
                'tags' => [
                    'slider' => [
                        'name' => 0,
                        'description' => 0,
                        'image' => 0,
                        'link' => 0,
                    ],
                    'advantage' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner1' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner2' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner3' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner4' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner5' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'banner6' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'faq' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'testimonial' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'action' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'news' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'service' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'featured' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'special' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'portfolio' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                    'specialist' => [
                        'name' => 1,
                        'description' => 1,
                        'image' => 1,
                        'link' => 1,
                    ],
                ],
                'keys' => [
                    'name',
                    'description',
                    'image',
                    'link',
                ],
            ],
            'social' => [
                'tags' => [
                    'social' => [
                        'vk' => 1,
                        'insta' => 1,
                    ],
                ],
                'keys' => [
                    'vk',
                    'insta',
                ],
                'oneline' => 1,
            ],
            'contact' => [
                'tags' => [
                    'contact' => [
                        'address' => 1,
                        'telephone1' => 1,
                        'telephone2' => 1,
                        'telephone3' => 1,
                        'email' => 1,
                        'emailrecipient' => 1,
                        'comment' => 1,
                        'open' => 1,
                        'coords' => 1,
                        'geocode' => 1,
                        'imagemap' => 1,
                    ],
                ],
                'keys' => [
                    'address',
                    'telephone1',
                    'telephone2',
                    'telephone3',
                    'email',
                    'emailrecipient',
                    'comment',
                    'open',
                    'coords',
                    'geocode',
                    'imagemap',
                ],
                'oneline' => 1,
            ],
            'seo' => [
                'tags' => [
                    'seohome' => 1,
                    'seocatalog' => 1,
                    'seoaction' => 1,
                    'seonews' => 1,
                    'seoservice' => 1,
                    'seocontact' => 1,
                    'seofaq' => 1,
                    'seoportfolio' => 1,
                    'seospecialist' => 1,
                    'seoprice' => 1,
                ],
                'keys' => [
                    'title',
                    'description',
                    'keyword',
                ],
            ],
        ];

        return $tags;
    }

}
