<?php
class ControllerInfoblockHomebanner6 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('infoblock/homebanner6');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('infoblock/homebanner6');

		$this->getList();
	}

	public function add() {
		$this->load->language('infoblock/homebanner6');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('infoblock/homebanner6');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_infoblock_homebanner6->addHomebanner6($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['iframe'])) {
				$url .= '&iframe=' . $this->request->get['iframe'];
			}


			$this->response->redirect($this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('infoblock/homebanner6');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('infoblock/homebanner6');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_infoblock_homebanner6->editHomebanner6($this->request->get['homebanner6_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['iframe'])) {
				$url .= '&iframe=' . $this->request->get['iframe'];
			}

			$this->response->redirect($this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('infoblock/homebanner6');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('infoblock/homebanner6');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $homebanner6_id) {
				$this->model_infoblock_homebanner6->deleteHomebanner6($homebanner6_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['iframe'])) {
				$url .= '&iframe=' . $this->request->get['iframe'];
			}

			$this->response->redirect($this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['iframe'])) {
			$this->document->addStyle('view/stylesheet/iframe.css');
			$this->document->addScript('view/javascript/iframe.js');
			$filter_iframe = $this->request->get['iframe'];
			$data['iframe'] = $this->request->get['iframe'];
		} else {
			$filter_iframe = '';
			$data['iframe'] = '';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['iframe'])) {
			$url .= '&iframe=' . $this->request->get['iframe'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('infoblock/homebanner6/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('infoblock/homebanner6/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		//$data['repair'] = $this->url->link('infoblock/homebanner6/repair', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['homebanners6'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$homebanner6_total = $this->model_infoblock_homebanner6->getTotalHomebanners6();

		$results = $this->model_infoblock_homebanner6->getHomebanners6($filter_data);

		foreach ($results as $result) {
			$data['homebanners6'][] = array(
				'homebanner6_id' => $result['homebanner6_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'edit'        => $this->url->link('infoblock/homebanner6/edit', 'user_token=' . $this->session->data['user_token'] . '&homebanner6_id=' . $result['homebanner6_id'] . $url, true),
				'delete'      => $this->url->link('infoblock/homebanner6/delete', 'user_token=' . $this->session->data['user_token'] . '&homebanner6_id=' . $result['homebanner6_id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_rebuild'] = $this->language->get('button_rebuild');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['iframe'])) {
			$url .= '&iframe=' . $this->request->get['iframe'];
		}

		$data['sort_name'] = $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . '&sort=sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['iframe'])) {
			$url .= '&iframe=' . $this->request->get['iframe'];
		}

		$pagination = new Pagination();
		$pagination->total = $homebanner6_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($homebanner6_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($homebanner6_total - $this->config->get('config_limit_admin'))) ? $homebanner6_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $homebanner6_total, ceil($homebanner6_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('infoblock/homebanner6_list', $data));
	}

	protected function getForm() {
		if (isset($this->request->get['iframe'])) {
			$this->document->addStyle('view/stylesheet/iframe.css');
			$this->document->addScript('view/javascript/iframe.js');
			$data['iframe'] = $this->request->get['iframe'];
		} else {
			$data['iframe'] = '';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['homebanner6_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_link'] = $this->language->get('entry_link');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['iframe'])) {
			$url .= '&iframe=' . $this->request->get['iframe'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['homebanner6_id'])) {
			$data['action'] = $this->url->link('infoblock/homebanner6/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('infoblock/homebanner6/edit', 'user_token=' . $this->session->data['user_token'] . '&homebanner6_id=' . $this->request->get['homebanner6_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('infoblock/homebanner6', 'user_token=' . $this->session->data['user_token'] . $url, true);

		if (isset($this->request->get['homebanner6_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$homebanner6_info = $this->model_infoblock_homebanner6->getHomebanner6($this->request->get['homebanner6_id']);
		}

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($homebanner6_info)) {
			$data['name'] = $homebanner6_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (!empty($homebanner6_info)) {
			$data['description'] = $homebanner6_info['description'];
		} else {
			$data['description'] = '';
		}

		if (isset($this->request->post['link'])) {
			$data['link'] = $this->request->post['link'];
		} elseif (!empty($homebanner6_info)) {
			$data['link'] = $homebanner6_info['link'];
		} else {
			$data['link'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($homebanner6_info)) {
			$data['image'] = $homebanner6_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($homebanner6_info) && is_file(DIR_IMAGE . $homebanner6_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($homebanner6_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($homebanner6_info)) {
			$data['sort_order'] = $homebanner6_info['sort_order'];
		} else {
			$data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($homebanner6_info)) {
			$data['status'] = $homebanner6_info['status'];
		} else {
			$data['status'] = true;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('infoblock/homebanner6_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'infoblock/homebanner6')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 255)) {
			$this->error['name'][1] = $this->language->get('error_name');
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'infoblock/homebanner6')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	/*public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('infoblock/homebanner6');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_infoblock_homebanner6->getRubric($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'homebanner6_id' => $result['homebanner6_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}*/
}
