<?php
// HTTP
define('HTTP_SERVER', 'http://opencart/admin/');
define('HTTP_CATALOG', 'http://opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart/admin/');
define('HTTPS_CATALOG', 'http://opencart/');

// DIR
// define('DIR_PROJECT', '/home/c/cd94006/opencart/');
define('DIR_PROJECT', '/var/www/opencart/');
define('DIR_APPLICATION', DIR_PROJECT . 'public_html/admin/');
define('DIR_SYSTEM', DIR_PROJECT . 'public_html/system/');
define('DIR_IMAGE', DIR_PROJECT . 'public_html/image/');
define('DIR_STORAGE', DIR_PROJECT . 'storage/');
define('DIR_CATALOG', DIR_PROJECT . '/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'username');
define('DB_PASSWORD', 'password');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
