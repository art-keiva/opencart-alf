<?php
class ModelCmsPrice extends Model {
    public function addPrice($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "price SET `key` = '" . $this->db->escape($data['key']) . "', `value` = '" . $this->db->escape($data['value']) . "'");

        $price_id = $this->db->getLastId();

        return $price_id;
    }

    public function getPrice($data = []) {
        $price_data = array();

        $sql = "SELECT * FROM " . DB_PREFIX . "price";

        if (isset($data['key'])) {
            $sql .= " WHERE `key` = '" . $data['key'] . "'";
        }

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $price_data[$result['key']] = $result['value'];
        }

        return $price_data;
    }

    public function editPrice($data) {


        $this->db->query("TRUNCATE " . DB_PREFIX . "price");

        foreach ($data as $key => $value) {
            $filter_data = [
                'key' => $key,
            ];

            $results = $this->getPrice($filter_data);

            if (!count($results)) {
                $this->addPrice([
                    'key' => $key,
                    'value' => $value,
                ]);
            }

            //$this->db->query("UPDATE " . DB_PREFIX . "price SET `value` = '" . $this->db->escape($value) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
        }
    }
}
