<?php
class ModelCmsPortfolio extends Model {
    public function addPortfolio($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "portfolio SET title = '" . $this->db->escape($data['title']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

        $portfolio_id = $this->db->getLastId();

        if (isset($data['portfolio_image'])) {
            foreach ($data['portfolio_image'] as $portfolio_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "portfolio_image SET portfolio_id = '" . (int)$portfolio_id . "', title = '" . $this->db->escape($portfolio_image['title']) . "', image = '" . $this->db->escape($portfolio_image['image']) . "', special = '" . (int)$portfolio_image['special'] . "', sort_order = '" . (int)$portfolio_image['sort_order'] . "'");
            }
        }

        $this->cache->delete('portfolio');

        return $portfolio_id;
    }

    public function editPortfolio($portfolio_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "portfolio SET title = '" . $this->db->escape($data['title']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE portfolio_id = '" . (int)$portfolio_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "portfolio_image WHERE portfolio_id = '" . (int)$portfolio_id . "'");

        if (isset($data['portfolio_image'])) {
            foreach ($data['portfolio_image'] as $portfolio_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "portfolio_image SET portfolio_id = '" . (int)$portfolio_id . "', title = '" . $this->db->escape($portfolio_image['title']) . "', image = '" . $this->db->escape($portfolio_image['image']) . "', special = '" . (int)$portfolio_image['special'] . "', sort_order = '" . (int)$portfolio_image['sort_order'] . "'");
            }
        }

        $this->cache->delete('portfolio');
    }

    public function deletePortfolio($portfolio_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "portfolio WHERE portfolio_id = '" . (int)$portfolio_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "portfolio_image WHERE portfolio_id = '" . (int)$portfolio_id . "'");

        $this->cache->delete('portfolio');
    }

    public function getPortfolio($portfolio_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "portfolio WHERE portfolio_id = '" . (int)$portfolio_id . "'");

        return $query->row;
    }

    public function getPortfolios($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "portfolio";

        $sort_data = array(
            'title',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY title";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getPortfolioImages($portfolio_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "portfolio_image WHERE portfolio_id = '" . (int)$portfolio_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalPortfolio() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "portfolio");

        return $query->row['total'];
    }
}