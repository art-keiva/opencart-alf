<?php
class ModelCmsService extends Model {
	public function addService($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "service SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "'");

		$service_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "service SET image = '" . $this->db->escape($data['image']) . "' WHERE service_id = '" . (int)$service_id . "'");
		}

		if (isset($data['image2'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "service SET image2 = '" . $this->db->escape($data['image2']) . "' WHERE service_id = '" . (int)$service_id . "'");
		}

		foreach ($data['service_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "service_description SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['service_store'])) {
			foreach ($data['service_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_to_store SET service_id = '" . (int)$service_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['service_image'])) {
			foreach ($data['service_image'] as $service_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_image SET service_id = '" . (int)$service_id . "', image = '" . $this->db->escape($service_image['image']) . "', sort_order = '" . (int)$service_image['sort_order'] . "'");
			}
		}

		// SEO URL
		if (isset($data['service_seo_url'])) {
			foreach ($data['service_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'service_id=" . (int)$service_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		if (isset($data['service_layout'])) {
			foreach ($data['service_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_to_layout SET service_id = '" . (int)$service_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('service');

		return $service_id;
	}

	public function editService($service_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "service SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "' WHERE service_id = '" . (int)$service_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "service SET image = '" . $this->db->escape($data['image']) . "' WHERE service_id = '" . (int)$service_id . "'");
		}

		if (isset($data['image2'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "service SET image2 = '" . $this->db->escape($data['image2']) . "' WHERE service_id = '" . (int)$service_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");

		foreach ($data['service_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "service_description SET service_id = '" . (int)$service_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "service_to_store WHERE service_id = '" . (int)$service_id . "'");

		if (isset($data['service_store'])) {
			foreach ($data['service_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_to_store SET service_id = '" . (int)$service_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "service_image WHERE service_id = '" . (int)$service_id . "'");

		if (isset($data['service_image'])) {
			foreach ($data['service_image'] as $service_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_image SET service_id = '" . (int)$service_id . "', image = '" . $this->db->escape($service_image['image']) . "', sort_order = '" . (int)$service_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'service_id=" . (int)$service_id . "'");

		if (isset($data['service_seo_url'])) {
			foreach ($data['service_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (trim($keyword)) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'service_id=" . (int)$service_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "service_to_layout WHERE service_id = '" . (int)$service_id . "'");

		if (isset($data['service_layout'])) {
			foreach ($data['service_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "service_to_layout SET service_id = '" . (int)$service_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('service');
	}

	public function deleteService($service_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "service WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_to_store WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_to_layout WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "service_image WHERE service_id = '" . (int)$service_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'service_id=" . (int)$service_id . "'");

		$this->cache->delete('service');
	}

	public function getService($service_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "service WHERE service_id = '" . (int)$service_id . "'");

		return $query->row;
	}

	public function getServices($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "service n LEFT JOIN " . DB_PREFIX . "service_description nd ON (n.service_id = nd.service_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$service_data = $this->cache->get('service.' . (int)$this->config->get('config_language_id'));

			if (!$service_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service n LEFT JOIN " . DB_PREFIX . "service_description nd ON (n.service_id = nd.service_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$service_data = $query->rows;

				$this->cache->set('service.' . (int)$this->config->get('config_language_id'), $service_data);
			}

			return $service_data;
		}
	}

	public function getServiceDescriptions($service_id) {
		$service_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_description WHERE service_id = '" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$service_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'anons'            => $result['anons'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $service_description_data;
	}

	public function getServiceStores($service_id) {
		$service_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_to_store WHERE service_id = '" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$service_store_data[] = $result['store_id'];
		}

		return $service_store_data;
	}

	public function getServiceImages($service_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_image WHERE service_id = '" . (int)$service_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getServiceSeoUrls($service_id) {
		$service_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'service_id=" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$service_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $service_seo_url_data;
	}

	public function getServiceLayouts($service_id) {
		$service_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_to_layout WHERE service_id = '" . (int)$service_id . "'");

		foreach ($query->rows as $result) {
			$service_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $service_layout_data;
	}

	public function getTotalService() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "service");

		return $query->row['total'];
	}

	public function getTotalServiceByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "service_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}