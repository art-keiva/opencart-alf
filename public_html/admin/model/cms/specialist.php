<?php
class ModelCmsSpecialist extends Model {
    public function addSpecialist($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "specialist SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "'");

        $specialist_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "specialist SET image = '" . $this->db->escape($data['image']) . "' WHERE specialist_id = '" . (int)$specialist_id . "'");
        }

        if (isset($data['image2'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "specialist SET image2 = '" . $this->db->escape($data['image2']) . "' WHERE specialist_id = '" . (int)$specialist_id . "'");
        }

        foreach ($data['specialist_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_description SET specialist_id = '" . (int)$specialist_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', link = '" . $this->db->escape($value['link']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['specialist_store'])) {
            foreach ($data['specialist_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_to_store SET specialist_id = '" . (int)$specialist_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['specialist_image'])) {
            foreach ($data['specialist_image'] as $specialist_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_image SET specialist_id = '" . (int)$specialist_id . "', image = '" . $this->db->escape($specialist_image['image']) . "', sort_order = '" . (int)$specialist_image['sort_order'] . "'");
            }
        }

        // SEO URL
        if (isset($data['specialist_seo_url'])) {
            foreach ($data['specialist_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'specialist_id=" . (int)$specialist_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        if (isset($data['specialist_layout'])) {
            foreach ($data['specialist_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_to_layout SET specialist_id = '" . (int)$specialist_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('specialist');

        return $specialist_id;
    }

    public function editSpecialist($specialist_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "specialist SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "' WHERE specialist_id = '" . (int)$specialist_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "specialist SET image = '" . $this->db->escape($data['image']) . "' WHERE specialist_id = '" . (int)$specialist_id . "'");
        }

        if (isset($data['image2'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "specialist SET image2 = '" . $this->db->escape($data['image2']) . "' WHERE specialist_id = '" . (int)$specialist_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_description WHERE specialist_id = '" . (int)$specialist_id . "'");

        foreach ($data['specialist_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_description SET specialist_id = '" . (int)$specialist_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', link = '" . $this->db->escape($value['link']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_to_store WHERE specialist_id = '" . (int)$specialist_id . "'");

        if (isset($data['specialist_store'])) {
            foreach ($data['specialist_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_to_store SET specialist_id = '" . (int)$specialist_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_image WHERE specialist_id = '" . (int)$specialist_id . "'");

        if (isset($data['specialist_image'])) {
            foreach ($data['specialist_image'] as $specialist_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_image SET specialist_id = '" . (int)$specialist_id . "', image = '" . $this->db->escape($specialist_image['image']) . "', sort_order = '" . (int)$specialist_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'specialist_id=" . (int)$specialist_id . "'");

        if (isset($data['specialist_seo_url'])) {
            foreach ($data['specialist_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (trim($keyword)) {
                        $this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'specialist_id=" . (int)$specialist_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_to_layout WHERE specialist_id = '" . (int)$specialist_id . "'");

        if (isset($data['specialist_layout'])) {
            foreach ($data['specialist_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialist_to_layout SET specialist_id = '" . (int)$specialist_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('specialist');
    }

    public function deleteSpecialist($specialist_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist WHERE specialist_id = '" . (int)$specialist_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_description WHERE specialist_id = '" . (int)$specialist_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_to_store WHERE specialist_id = '" . (int)$specialist_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_to_layout WHERE specialist_id = '" . (int)$specialist_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialist_image WHERE specialist_id = '" . (int)$specialist_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'specialist_id=" . (int)$specialist_id . "'");

        $this->cache->delete('specialist');
    }

    public function getSpecialist($specialist_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "specialist WHERE specialist_id = '" . (int)$specialist_id . "'");

        return $query->row;
    }

    public function getSpecialists($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "specialist n LEFT JOIN " . DB_PREFIX . "specialist_description nd ON (n.specialist_id = nd.specialist_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sort_data = array(
                'nd.title',
                'n.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY nd.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $specialist_data = $this->cache->get('specialist.' . (int)$this->config->get('config_language_id'));

            if (!$specialist_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist n LEFT JOIN " . DB_PREFIX . "specialist_description nd ON (n.specialist_id = nd.specialist_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

                $specialist_data = $query->rows;

                $this->cache->set('specialist.' . (int)$this->config->get('config_language_id'), $specialist_data);
            }

            return $specialist_data;
        }
    }

    public function getSpecialistDescriptions($specialist_id) {
        $specialist_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_description WHERE specialist_id = '" . (int)$specialist_id . "'");

        foreach ($query->rows as $result) {
            $specialist_description_data[$result['language_id']] = array(
                'title'            => $result['title'],
                'anons'            => $result['anons'],
                'description'      => $result['description'],
                'link'             => $result['link'],
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword']
            );
        }

        return $specialist_description_data;
    }

    public function getSpecialistStores($specialist_id) {
        $specialist_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_to_store WHERE specialist_id = '" . (int)$specialist_id . "'");

        foreach ($query->rows as $result) {
            $specialist_store_data[] = $result['store_id'];
        }

        return $specialist_store_data;
    }

    public function getSpecialistImages($specialist_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_image WHERE specialist_id = '" . (int)$specialist_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getSpecialistSeoUrls($specialist_id) {
        $specialist_seo_url_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'specialist_id=" . (int)$specialist_id . "'");

        foreach ($query->rows as $result) {
            $specialist_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
        }

        return $specialist_seo_url_data;
    }

    public function getSpecialistLayouts($specialist_id) {
        $specialist_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_to_layout WHERE specialist_id = '" . (int)$specialist_id . "'");

        foreach ($query->rows as $result) {
            $specialist_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $specialist_layout_data;
    }

    public function getTotalSpecialist() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "specialist");

        return $query->row['total'];
    }

    public function getTotalSpecialistByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "specialist_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }
}