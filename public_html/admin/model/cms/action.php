<?php
class ModelCmsAction extends Model {
	public function addAction($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "action SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "'");

		$action_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "action SET image = '" . $this->db->escape($data['image']) . "' WHERE action_id = '" . (int)$action_id . "'");
		}

		foreach ($data['action_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['action_store'])) {
			foreach ($data['action_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "action_to_store SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		// SEO URL
		if (isset($data['action_seo_url'])) {
			foreach ($data['action_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		if (isset($data['action_layout'])) {
			foreach ($data['action_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "action_to_layout SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('action');

		return $action_id;
	}

	public function editAction($action_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "action SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', special = '" . (int)$data['special'] . "', date_added = '" . $this->db->escape($data['date_added']) . "' WHERE action_id = '" . (int)$action_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "action SET image = '" . $this->db->escape($data['image']) . "' WHERE action_id = '" . (int)$action_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");

		foreach ($data['action_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "action_description SET action_id = '" . (int)$action_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', anons = '" . $this->db->escape($value['anons']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");

		if (isset($data['action_store'])) {
			foreach ($data['action_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "action_to_store SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'action_id=" . (int)$action_id . "'");

		if (isset($data['action_seo_url'])) {
			foreach ($data['action_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (trim($keyword)) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'action_id=" . (int)$action_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");

		if (isset($data['action_layout'])) {
			foreach ($data['action_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "action_to_layout SET action_id = '" . (int)$action_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('action');
	}

	public function deleteAction($action_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'action_id=" . (int)$action_id . "'");

		$this->cache->delete('action');
	}

	public function getAction($action_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "action WHERE action_id = '" . (int)$action_id . "'");

		return $query->row;
	}

	public function getActions($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON (n.action_id = nd.action_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$action_data = $this->cache->get('action.' . (int)$this->config->get('config_language_id'));

			if (!$action_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON (n.action_id = nd.action_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$action_data = $query->rows;

				$this->cache->set('action.' . (int)$this->config->get('config_language_id'), $action_data);
			}

			return $action_data;
		}
	}

	public function getActionDescriptions($action_id) {
		$action_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_description WHERE action_id = '" . (int)$action_id . "'");

		foreach ($query->rows as $result) {
			$action_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'anons'            => $result['anons'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $action_description_data;
	}

	public function getActionStores($action_id) {
		$action_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_store WHERE action_id = '" . (int)$action_id . "'");

		foreach ($query->rows as $result) {
			$action_store_data[] = $result['store_id'];
		}

		return $action_store_data;
	}

	public function getActionSeoUrls($action_id) {
		$action_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'action_id=" . (int)$action_id . "'");

		foreach ($query->rows as $result) {
			$action_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $action_seo_url_data;
	}

	public function getActionLayouts($action_id) {
		$action_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "'");

		foreach ($query->rows as $result) {
			$action_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $action_layout_data;
	}

	public function getTotalAction() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action");

		return $query->row['total'];
	}

	public function getTotalActionByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "action_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}