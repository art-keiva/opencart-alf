<?php
class ModelDesignSeoUrl extends Model {
    public function addSeoUrl($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$data['store_id'] . "', language_id = '" . (int)$data['language_id'] . "', query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
    }

    public function editSeoUrl($seo_url_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$data['store_id'] . "', language_id = '" . (int)$data['language_id'] . "', query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "' WHERE seo_url_id = '" . (int)$seo_url_id . "'");
    }

    public function deleteSeoUrl($seo_url_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE seo_url_id = '" . (int)$seo_url_id . "'");
    }

    public function getSeoUrl($seo_url_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE seo_url_id = '" . (int)$seo_url_id . "'");

        return $query->row;
    }

    public function getSeoUrls($data = array()) {
        $sql = "SELECT *, (SELECT `name` FROM `" . DB_PREFIX . "store` s WHERE s.store_id = su.store_id) AS store, (SELECT `name` FROM `" . DB_PREFIX . "language` l WHERE l.language_id = su.language_id) AS language FROM `" . DB_PREFIX . "seo_url` su";

        $implode = array();

        if (!empty($data['filter_query'])) {
            $implode[] = "`query` LIKE '" . $this->db->escape($data['filter_query']) . "'";
        }

        if (!empty($data['filter_keyword'])) {
            $implode[] = "`keyword` LIKE '" . $this->db->escape($data['filter_keyword']) . "'";
        }

        if (isset($data['filter_store_id']) && $data['filter_store_id'] !== '') {
            $implode[] = "`store_id` = '" . (int)$data['filter_store_id'] . "'";
        }

        if (!empty($data['filter_language_id']) && $data['filter_language_id'] !== '') {
            $implode[] = "`language_id` = '" . (int)$data['filter_language_id'] . "'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'query',
            'keyword',
            'language_id',
            'store_id'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY query";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalSeoUrls($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "seo_url`";

        $implode = array();

        if (!empty($data['filter_query'])) {
            $implode[] = "query LIKE '" . $this->db->escape($data['filter_query']) . "'";
        }

        if (!empty($data['filter_keyword'])) {
            $implode[] = "keyword LIKE '" . $this->db->escape($data['filter_keyword']) . "'";
        }

        if (!empty($data['filter_store_id']) && $data['filter_store_id'] !== '') {
            $implode[] = "store_id = '" . (int)$data['filter_store_id'] . "'";
        }

        if (!empty($data['filter_language_id']) && $data['filter_language_id'] !== '') {
            $implode[] = "language_id = '" . (int)$data['filter_language_id'] . "'";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getSeoUrlsByKeyword($keyword) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE keyword = '" . $this->db->escape($keyword) . "'");

        return $query->rows;
    }

    public function getSeoUrlsByQuery($keyword) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seo_url` WHERE keyword = '" . $this->db->escape($keyword) . "'");

        return $query->rows;
    }

    public function getAutoPrefixUrl($data) {
        $keyword = $data['keyword'];

        $seo_urls = $this->getSeoUrlsByKeyword($data['keyword']);

        foreach ($seo_urls as $seo_url) {
            if (($seo_url['store_id'] == $data['store_id']) && (!isset($data['entity_id']) || ($seo_url['query'] != $data['entity'] . '_id=' . $data['entity_id']))) {
                $path = '';
                $parts = explode('_', (string)$data['keyword']);
                $prefix = (int)array_pop($parts);
                $prefix++;

                if (count($parts) > 0) {
                    foreach ($parts as $path_part) {
                        if (!$path) {
                            $path = $path_part;
                        } else {
                            $path .= '_' . $path_part;
                        }
                    }

                    $path .= '_' . $prefix;
                } else {
                    $path = $data['keyword'] . '_' . $prefix;
                }

                $data2 = array(
                    'store_id'    => $data['store_id'],
                    'language_id' => $data['language_id'],
                    'keyword'     => $path,  // edited keyword
                    //'entity'      => $data['entity'],
                    //'entity_id'   => $data['entity_id'],
                );

                if (isset($data['entity'])) {
                    $data2['entity'] = $data['entity'];
                }
                if (isset($data['entity_id'])) {
                    $data2['entity_id'] = $data['entity_id'];
                }

                $keyword = $this->getAutoPrefixUrl($data2); // check if exists again
            }
        }

        return $keyword;
    }

    public function setAutoPrefixUrl($seo_url_id) {
        $result = $this->getSeoUrl($seo_url_id);

        if (!empty($result)) {
            $path = '';
            $parts = explode('_', (string)$result['keyword']);
            $prefix = (int)array_pop($parts);

            if (count($parts) > 0) {
                foreach ($parts as $path_part) {
                    if (!$path) {
                        $path = $path_part;
                    } else {
                        $path .= '_' . $path_part;
                    }
                }
            } else {
                $path = $result['keyword'];
            }

            $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE seo_url_id = '" . (int)$result['seo_url_id'] . "'");

            $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET seo_url_id = '" . (int)$result['seo_url_id'] . "', store_id = '" . (int)$result['store_id'] . "', language_id = '" . (int)$result['language_id'] . "', query = '" . $this->db->escape($result['query']) . "', keyword = '" . $this->db->escape($path) . '_' . (int)$result['seo_url_id'] . "'");

            // $this->db->query("UPDATE `" . DB_PREFIX . "seo_url` SET keyword = '" . $this->db->escape($path) . '_' . (int)$result['seo_url_id'] . "' WHERE seo_url_id = '" . (int)$result['seo_url_id'] . "'");
        }
    }

    public function translit($value = '') {
        $converter = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

            'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
            'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
            'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
            'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
            'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
            'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
            'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',

            // '&' => '-and-',
        );

        $value = strtr($value, $converter);

        return $value;
    }

    public function translit_sef($value = '') {
        /*
            fromChars  : 'абвгдезиклмнопрстуфыэйхёц',
            toChars    : 'abvgdeziklmnoprstufyeyhec',
            biChars    : {'ж':'zh','ч':'ch','ш':'sh','щ':'sch','ю':'yu','я':'ya','&':'-and-'},
            vowelChars : 'аеёиоуыэюя',
        */
        $converter = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

            // '&' => '-and-',
        );

        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '-', $value);
        $value = trim($value, '-');

        $value = strtr($value, $converter);

        return $value;
    }
}