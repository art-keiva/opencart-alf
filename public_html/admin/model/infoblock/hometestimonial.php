<?php
class ModelInfoblockHometestimonial extends Model {
	public function addHometestimonial($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "hometestimonial SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', job = '" . $this->db->escape($data['job']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$hometestimonial_id = $this->db->getLastId();
		$this->cache->delete('hometestimonial');

		return $hometestimonial_id;
	}

	public function editHometestimonial($hometestimonial_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "hometestimonial SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', job = '" . $this->db->escape($data['job']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE hometestimonial_id = '" . (int)$hometestimonial_id . "'");

		$this->cache->delete('hometestimonial');
	}

	public function deleteHometestimonial($hometestimonial_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "hometestimonial WHERE hometestimonial_id = '" . (int)$hometestimonial_id . "'");

		$this->cache->delete('hometestimonial');
	}

	public function getHometestimonial($hometestimonial_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "hometestimonial WHERE hometestimonial_id = '" . (int)$hometestimonial_id . "'");

		return $query->row;
	}

	public function getHometestimonials($data = array()) {
		$sql = "SELECT hometestimonial_id, name, sort_order FROM " . DB_PREFIX . "hometestimonial";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	/*public function getRubric($data = array()) {
		$sql = "SELECT cp.rubrics_id AS rubrics_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "rubrics_path cp LEFT JOIN " . DB_PREFIX . "rubrics c1 ON (cp.rubrics_id = c1.rubrics_id) LEFT JOIN " . DB_PREFIX . "rubrics c2 ON (cp.path_id = c2.rubrics_id) LEFT JOIN " . DB_PREFIX . "rubrics_description cd1 ON (cp.path_id = cd1.rubrics_id) LEFT JOIN " . DB_PREFIX . "rubrics_description cd2 ON (cp.rubrics_id = cd2.rubrics_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.rubrics_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getRubricsDescriptions($rubrics_id) {
		$rubrics_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rubrics_description WHERE rubrics_id = '" . (int)$rubrics_id . "'");

		foreach ($query->rows as $result) {
			$rubrics_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $rubrics_description_data;
	}*/

	public function getTotalHometestimonials() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "hometestimonial");

		return $query->row['total'];
	}
}





















