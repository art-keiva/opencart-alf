<?php
class ModelInfoblockHomebanner2 extends Model {
	public function addHomebanner2($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "homebanner2 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$homebanner2_id = $this->db->getLastId();
		$this->cache->delete('homebanner2');

		return $homebanner2_id;
	}

	public function editHomebanner2($homebanner2_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "homebanner2 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE homebanner2_id = '" . (int)$homebanner2_id . "'");

		$this->cache->delete('homebanner2');
	}

	public function deleteHomebanner2($homebanner2_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "homebanner2 WHERE homebanner2_id = '" . (int)$homebanner2_id . "'");

		$this->cache->delete('homebanner2');
	}

	public function getHomebanner2($homebanner2_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner2 WHERE homebanner2_id = '" . (int)$homebanner2_id . "'");

		return $query->row;
	}

	public function getHomebanners2($data = array()) {
		$sql = "SELECT homebanner2_id, name, sort_order FROM " . DB_PREFIX . "homebanner2";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalHomebanners2() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homebanner2");

		return $query->row['total'];
	}
}





















