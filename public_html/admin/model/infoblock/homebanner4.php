<?php
class ModelInfoblockHomebanner4 extends Model {
	public function addHomebanner4($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "homebanner4 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$homebanner4_id = $this->db->getLastId();
		$this->cache->delete('homebanner4');

		return $homebanner4_id;
	}

	public function editHomebanner4($homebanner4_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "homebanner4 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE homebanner4_id = '" . (int)$homebanner4_id . "'");

		$this->cache->delete('homebanner4');
	}

	public function deleteHomebanner4($homebanner4_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "homebanner4 WHERE homebanner4_id = '" . (int)$homebanner4_id . "'");

		$this->cache->delete('homebanner4');
	}

	public function getHomebanner4($homebanner4_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner4 WHERE homebanner4_id = '" . (int)$homebanner4_id . "'");

		return $query->row;
	}

	public function getHomebanners4($data = array()) {
		$sql = "SELECT homebanner4_id, name, sort_order FROM " . DB_PREFIX . "homebanner4";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalHomebanners4() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homebanner4");

		return $query->row['total'];
	}
}





















