<?php
class ModelInfoblockInfoblock extends Model {
	public function addInfoblock($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "infoblock SET `key` = '" . $this->db->escape($data['key']) . "', `value` = '" . $this->db->escape($data['value']) . "'");

        $infoblock_id = $this->db->getLastId();

		return $infoblock_id;
	}

	public function getInfoblock($data = []) {
		$infoblock_data = array();

		$sql = "SELECT * FROM " . DB_PREFIX . "infoblock";

        if (isset($data['key'])) {
            $sql .= " WHERE `key` = '" . $data['key'] . "'";
        }

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$infoblock_data[$result['key']] = $result['value'];
		}

		return $infoblock_data;
	}

	public function editInfoblock($data) {

		foreach ($data as $key => $value) {
			if ($key == 'infoblock_download_catalog') {
				// echo '<pre>'; var_dump($key); echo '</pre>';
				// echo '<pre>'; var_dump($value); echo '</pre>';
				$this->load->model('tool/download');

				$old_download = $this->model_tool_download->getDownload($value['download_id']);

				if (empty($old_download) || ($old_download['filename'] !== $value['filename'])) {
					if (empty($value['filename'])) {
						$this->model_tool_download->deleteDownload($value['download_id']);
						$download_id = '';
					} else {
						$value['download_description'][(int)$this->config->get('config_language_id')]['name'] = $value['mask'];

						$download_id = $this->model_tool_download->addDownload($value);

					}

					$this->db->query("UPDATE " . DB_PREFIX . "infoblock SET `value` = '" . $download_id . "' WHERE `key` = '" . $this->db->escape($key) . "'");

					// $this->model_setting_setting->editSettingValue('config', 'config_download_catalog', $download_id);
				}

				continue;
			}

            if ($key === 'infoblock_contact_emailrecipient') {
                $this->load->model('setting/setting');

                $this->model_setting_setting->editSettingValue('config', 'config_email', $value);
            }

			$this->db->query("UPDATE " . DB_PREFIX . "infoblock SET `value` = '" . $this->db->escape($value) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
		}

		if (isset($data['catalog_download']) and (null !== $this->config->get('config_download_catalog'))) {
			$this->load->model('tool/download');
			$this->load->model('setting/setting');

			$old_download = $this->model_tool_download->getDownload($this->config->get('config_download_catalog'));

			if (empty($old_download) || $old_download['filename'] !== $data['catalog_download']['filename']) {
				if (empty($data['catalog_download']['filename'])) {
					$this->model_tool_download->deleteDownload($this->config->get('config_download_catalog'));
					$download_id = '';
				} else {
					$data['catalog_download']['download_description'][(int)$this->config->get('config_language_id')]['name'] = $data['catalog_download']['mask'];

					$download_id = $this->model_tool_download->addDownload($data['catalog_download']);

				}

				$this->model_setting_setting->editSettingValue('config', 'config_download_catalog', $download_id);
			}
		}
	}
}
