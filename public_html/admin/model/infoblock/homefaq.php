<?php
class ModelInfoblockHomefaq extends Model {
	public function addHomefaq($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "homefaq SET name = '" . $this->db->escape($data['name']) . "', question = '" . $this->db->escape($data['question']) . "', answer = '" . $this->db->escape($data['answer']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$homefaq_id = $this->db->getLastId();
		$this->cache->delete('homefaq');

		return $homefaq_id;
	}

	public function editHomefaq($homefaq_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "homefaq SET name = '" . $this->db->escape($data['name']) . "', question = '" . $this->db->escape($data['question']) . "', answer = '" . $this->db->escape($data['answer']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE homefaq_id = '" . (int)$homefaq_id . "'");

		$this->cache->delete('homefaq');
	}

	public function deleteHomefaq($homefaq_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "homefaq WHERE homefaq_id = '" . (int)$homefaq_id . "'");

		$this->cache->delete('homefaq');
	}

	public function getHomefaq($homefaq_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homefaq WHERE homefaq_id = '" . (int)$homefaq_id . "'");

		return $query->row;
	}

	public function getHomefaqs($data = array()) {
		$sql = "SELECT homefaq_id, name, sort_order FROM " . DB_PREFIX . "homefaq";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalHomefaqs() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homefaq");

		return $query->row['total'];
	}
}





















