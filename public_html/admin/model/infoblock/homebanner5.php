<?php
class ModelInfoblockHomebanner5 extends Model {
	public function addHomebanner5($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "homebanner5 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', manipulator = '" . (int)$data['manipulator'] . "', weight = '" . (float)$data['weight'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', indent = '" . (float)$data['indent'] . "', fuel_need = '" . (float)$data['fuel_need'] . "', fuel_cost = '" . (float)$data['fuel_cost'] . "', amortization = '" . (float)$data['amortization'] . "', interest_seller = '" . (float)$data['interest_seller'] . "', tariff_driver = '" . (float)$data['tariff_driver'] . "', tariff_landing = '" . (float)$data['tariff_landing'] . "'");

		$homebanner5_id = $this->db->getLastId();
		$this->cache->delete('homebanner5');

		return $homebanner5_id;
	}

	public function editHomebanner5($homebanner5_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "homebanner5 SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', manipulator = '" . (int)$data['manipulator'] . "', weight = '" . (float)$data['weight'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', indent = '" . (float)$data['indent'] . "', fuel_need = '" . (float)$data['fuel_need'] . "', fuel_cost = '" . (float)$data['fuel_cost'] . "', amortization = '" . (float)$data['amortization'] . "', interest_seller = '" . (float)$data['interest_seller'] . "', tariff_driver = '" . (float)$data['tariff_driver'] . "', tariff_landing = '" . (float)$data['tariff_landing'] . "' WHERE homebanner5_id = '" . (int)$homebanner5_id . "'");

		$this->cache->delete('homebanner5');
	}

	public function deleteHomebanner5($homebanner5_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "homebanner5 WHERE homebanner5_id = '" . (int)$homebanner5_id . "'");

		$this->cache->delete('homebanner5');
	}

	public function getHomebanner5($homebanner5_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner5 WHERE homebanner5_id = '" . (int)$homebanner5_id . "'");

		return $query->row;
	}

	public function getHomebanners5($data = array()) {
		$sql = "SELECT homebanner5_id, name, sort_order FROM " . DB_PREFIX . "homebanner5";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalHomebanners5() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homebanner5");

		return $query->row['total'];
	}
}





















