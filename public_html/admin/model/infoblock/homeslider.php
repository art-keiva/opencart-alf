<?php
class ModelInfoblockHomeslider extends Model {
	public function addHomeslider($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "homeslider SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', image2 = '" . $this->db->escape($data['image2']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$homeslider_id = $this->db->getLastId();

		$this->cache->delete('homeslider');

		return $homeslider_id;
	}

	public function editHomeslider($homeslider_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "homeslider SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', link = '" . $this->db->escape($data['link']) . "', image = '" . $this->db->escape($data['image']) . "', image2 = '" . $this->db->escape($data['image2']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE homeslider_id = '" . (int)$homeslider_id . "'");

		$this->cache->delete('homeslider');
	}

	public function deleteHomeslider($homeslider_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "homeslider WHERE homeslider_id = '" . (int)$homeslider_id . "'");

		$this->cache->delete('homeslider');
	}

	public function getHomeslider($homeslider_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homeslider WHERE homeslider_id = '" . (int)$homeslider_id . "'");

		return $query->row;
	}

	public function getHomesliders($data = array()) {
		$sql = "SELECT homeslider_id, name, sort_order FROM " . DB_PREFIX . "homeslider";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalHomesliders() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homeslider");

		return $query->row['total'];
	}
}





















