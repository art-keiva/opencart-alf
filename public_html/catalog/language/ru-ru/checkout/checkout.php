<?php
// Heading
$_['heading_title']                  = 'Оформление заказа';

// Text
$_['text_cart']                      = 'Корзина покупок';
$_['text_checkout_option']           = 'Шаг %s: Способ оформления заказа';
$_['text_checkout_account']          = 'Шаг %s: Профиль &amp; Платежная информация';
$_['text_checkout_payment_address']  = 'Шаг %s: Платежная информация';
$_['text_checkout_shipping_address'] = 'Шаг %s: Адрес доставки';
$_['text_checkout_shipping_method']  = 'Шаг %s: Способ доставки';
$_['text_checkout_payment_method']   = 'Шаг %s: Способ оплаты';
$_['text_checkout_confirm']          = 'Шаг %s: Подтверждение заказа';
$_['text_modify']                    = 'Изменить &raquo;';
$_['text_new_customer']              = 'Новый покупатель';
$_['text_returning_customer']        = 'Зарегистрированный пользователь';
$_['text_checkout']                  = 'Варианты Оформления заказа';
$_['text_i_am_returning_customer']   = 'Я совершал здесь покупки ранее и регистрировался';
$_['text_register']                  = 'Регистрация';
$_['text_guest']                     = 'Оформить заказ без регистрации';
$_['text_register_account']          = 'Создание учетной записи поможет делать покупки быстрее и удобнее, а так же получать скидки как постоянный покупатель.';
$_['text_forgotten']                 = 'Забыли пароль?';
$_['text_your_details']              = 'Личные данные';
$_['text_your_address']              = 'Адрес';
$_['text_your_password']             = 'Пароль';
$_['text_agree']                     = 'Я прочитал <a href="%s" target="_blank"><b>%s</b></a> и согласен с условиями безопасности и обработки персональных данных';
$_['text_address_new']               = 'Я хочу использовать новый адрес';
$_['text_address_existing']          = 'Я хочу использовать существующий адрес';
$_['text_shipping_method']           = 'Выберите удобный способ доставки для данного заказа';
$_['text_payment_method']            = 'Выберите способ оплаты для данного заказа';
$_['text_comments']                  = 'Вы можете добавить комментарий к своему заказу';
$_['text_recurring_item']            = 'Элемент периодичности';
$_['text_payment_recurring']         = 'Платежный профиль';
$_['text_trial_description']         = 'Стоимость: %s; Периодичность: %d %s; Кол-во платежей: %d;  Далее,  ';
$_['text_payment_description']       = 'Стоимость: %s; Периодичность: %d %s; Кол-во платежей: %d';
$_['text_payment_cancel']            = '%s каждые %d %s(й) пока не отменен';
$_['text_day']                       = 'день';
$_['text_week']                      = 'неделю';
$_['text_semi_month']                = 'полмесяца';
$_['text_month']                     = 'месяц';
$_['text_year']                      = 'год';

// Column
$_['column_name']                    = 'Название товара';
$_['column_model']                   = 'Модель';
$_['column_quantity']                = 'Количество';
$_['column_price']                   = 'Цена';
$_['column_total']                   = 'Итого';

// Entry
$_['entry_email_address']            = 'E-Mail Адрес';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Пароль';
$_['entry_confirm']                  = 'Подтверждение пароля';
$_['entry_firstname']                = 'Имя';
$_['entry_lastname']                 = 'Фамилия';
$_['entry_telephone']                = 'Телефон';
$_['entry_address']                  = 'Выберите адрес';
$_['entry_company']                  = 'Компания';
$_['entry_customer_group']           = 'Тип бизнеса';
$_['entry_address_1']                = 'Адрес';
$_['entry_address_2']                = 'Адрес 2';
$_['entry_postcode']                 = 'Индекс';
$_['entry_city']                     = 'Город';
$_['entry_country']                  = 'Страна';
$_['entry_zone']                     = 'Регион / Область';
$_['entry_newsletter']               = 'Я хочу подписаться на Новости %s .';
$_['entry_shipping']                 = 'Мой адрес доставки совпадает с платежным.';


$_['text_success']                 = 'Спасибо за заказ, с Вами свяжется продавец товара или услуги.';
$_['text_fail']                 = 'Возникла проблема при обработке Вашего заказа! Если проблема возникает повторно, <a href="%s">свяжитесь с администратором сайта</a>.';
$_['text_price']                 = 'р.';


$_['email_subject_admins']  = 'Вы получили заказ!';
$_['email_subject_customer']  = 'Ваш заказ успешно оформлен!';

// send emails
$_['text_subject']      = '%s - Заказ %s';
$_['text_received']     = 'Вы получили заказ.';
$_['text_order_id']     = '№ заказа:';
$_['text_date_added']   = 'Дата заказа:';
$_['text_order_status'] = 'Состояние заказа:';
$_['text_product']      = 'Товары';
$_['text_service']      = 'Услуги';
$_['text_total']        = 'Итого';
$_['text_payment']      = 'Cпособ оплаты';
$_['text_payment_cash']      = 'Оплата наличными';
$_['text_payment_bank']      = 'Банковский перевод';
$_['text_shipping']      = 'Cпособ доставки';
$_['text_shipping_pickup']      = 'Самовывоз';
$_['text_shipping_delivery']      = 'Доставка с оплатой';
$_['text_name']                     = 'ФИО';
$_['text_email']                    = 'Email';
$_['text_phone']                    = 'Телефон';
$_['text_address']                  = 'Адрес';
$_['text_city']                     = 'Город';
$_['text_code']                     = 'Индекс';
$_['text_country']               = 'Страна';
$_['text_zone']                  = 'Регион / Область';
$_['text_comment']                  = 'Комментарии';

// Error
$_['error_warning']                  = 'Внимательно проверьте форму на ошибки.';
$_['error_name']                     = 'ФИО не может содержать меньше 3 и больше 32 символов';
$_['error_email']                    = 'Неправильно указан email адрес.';
$_['error_phone']                    = 'Телефон не может быть пустым';
$_['error_address']                  = 'Адрес не может быть пустым';
$_['error_city']                     = 'Город не может быть пустым';
$_['error_code']                     = 'Индекс не может быть пустым';
$_['error_country_id']               = 'Выберите страну';
$_['error_zone_id']                  = 'Выберите регион / область';
$_['error_comment']                  = 'Комментарий не может содержать больше 150 символов';
$_['error_agree']                    = 'Примите условия соглашения';
$_['error_captcha']                  = 'Проверка капчи не пройдена';

