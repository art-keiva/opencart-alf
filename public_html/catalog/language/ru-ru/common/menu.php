<?php
// Text
$_['text_all']     = 'Показать все';
$_['text_home']    = 'Главная';
$_['text_company'] = 'Клиники';
$_['text_product'] = 'Товары';
$_['text_service'] = 'Услуги';
$_['text_vacancy'] = 'Вакансии';
$_['text_specialist'] = 'Специалисты';
$_['text_action']  = 'Акции';
$_['text_site']    = 'Сайты';
$_['text_news']    = 'Новости';
$_['text_article'] = 'Статьи';
$_['text_price'] = 'Цены';
$_['text_help']    = 'Помощь';
$_['text_information']    = 'Информация';
$_['text_contact']    = 'Контакты';
$_['text_account']    = 'Аккаунт';

// Child home
$_['text_region']  = 'Регионы России';
$_['text_faq']     = 'Вопросы и ответы';
$_['text_rating']  = 'Отзывы';
$_['text_reclame'] = 'Реклама';
$_['text_sitemap'] = 'Карта сайта';
