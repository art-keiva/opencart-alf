<?php
class ModelInfoblockHomeslider extends Model {
	public function getHomeslider($homeslider_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homeslider WHERE homeslider_id = '" . (int)$homeslider_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomesliders() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homeslider WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}