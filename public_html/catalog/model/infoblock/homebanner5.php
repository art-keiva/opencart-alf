<?php
class ModelInfoblockHomebanner5 extends Model {
	public function getHomebanner5($homebanner5_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner5 WHERE homebanner5_id = '" . (int)$homebanner5_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner5s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner5 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}