<?php
class ModelInfoblockHometestimonial extends Model {
	public function getHometestimonial($hometestimonial_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "hometestimonial WHERE hometestimonial_id = '" . (int)$hometestimonial_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHometestimonials() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "hometestimonial WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}