<?php
class ModelInfoblockHomebanner1 extends Model {
	public function getHomebanner1($homebanner1_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner1 WHERE homebanner1_id = '" . (int)$homebanner1_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner1s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner1 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}