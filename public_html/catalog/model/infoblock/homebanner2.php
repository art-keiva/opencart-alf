<?php
class ModelInfoblockHomebanner2 extends Model {
	public function getHomebanner2($homebanner2_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner2 WHERE homebanner2_id = '" . (int)$homebanner2_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner2s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner2 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}