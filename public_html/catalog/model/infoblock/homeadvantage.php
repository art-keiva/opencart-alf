<?php
class ModelInfoblockHomeadvantage extends Model {
	public function getHomeadvantage($advantage_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homeadvantage WHERE homeadvantage_id = '" . (int)$advantage_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomeadvantages() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homeadvantage WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}