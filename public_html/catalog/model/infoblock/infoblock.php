<?php
class ModelInfoblockInfoblock extends Model {
    public function getInfoblock() {
        $infoblock_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "infoblock");

        foreach ($query->rows as $result) {
            $infoblock_data[$result['key']] = $result['value'];
        }

        return $infoblock_data;
    }

    public function getInfoblocksSplittedByTag() {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "infoblock`");

        $infoblockByTags = [];

        foreach ($query->rows as $result) {
            $tagKey = explode('_', $result['key']);

            if (isset($tagKey[1])) {
                $tag = $tagKey[1];

                if (!isset($infoblockByTags[$tag])) {
                    $infoblockByTags[$tag] = [];
                }

                $infoblockByTags[$tag][$result['key']] = $result['value'];
            }
        }

        return $infoblockByTags;
    }

    public function getInfoblockByKey($key) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "infoblock` WHERE `key` = '" . $this->db->escape($key) . "'");

        return $query->row;
    }
}
