<?php
class ModelInfoblockHomebanner6 extends Model {
	public function getHomebanner6($homebanner6_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner6 WHERE homebanner6_id = '" . (int)$homebanner6_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner6s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner6 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}