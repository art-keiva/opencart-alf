<?php
class ModelInfoblockHomebanner4 extends Model {
	public function getHomebanner4($homebanner4_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner4 WHERE homebanner4_id = '" . (int)$homebanner4_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner4s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner4 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}