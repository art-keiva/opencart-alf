<?php
class ModelInfoblockHomefaq extends Model {
	public function getHomefaq($homefaq_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homefaq WHERE homefaq_id = '" . (int)$homefaq_id . "' AND status = '1'");

		return $query->row;
	}

    // public function getHomefaqs() {
    //     $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homefaq WHERE status = '1' ORDER BY sort_order ASC");

    //     return $query->rows;
    // }

    public function getMainPageHomefaqs() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homefaq WHERE status = '1' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getHomefaqs() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homefaq ORDER BY sort_order ASC");

        return $query->rows;
    }
}