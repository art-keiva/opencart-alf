<?php
class ModelInfoblockHomebanner3 extends Model {
	public function getHomebanner3($homebanner3_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homebanner3 WHERE homebanner3_id = '" . (int)$homebanner3_id . "' AND status = '1'");

		return $query->row;
	}

	public function getHomebanner3s() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homebanner3 WHERE status = '1' ORDER BY sort_order ASC");

		return $query->rows;
	}
}