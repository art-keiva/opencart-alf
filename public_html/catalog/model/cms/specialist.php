<?php
class ModelCmsSpecialist extends Model {
    public function getSpecialist($specialist_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "specialist n LEFT JOIN " . DB_PREFIX . "specialist_description nd ON (n.specialist_id = nd.specialist_id) LEFT JOIN " . DB_PREFIX . "specialist_to_store n2s ON (n.specialist_id = n2s.specialist_id) WHERE n.specialist_id = '" . (int)$specialist_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");

        return $query->row;
    }

    public function getSpecialists($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "specialist n LEFT JOIN " . DB_PREFIX . "specialist_description nd ON (n.specialist_id = nd.specialist_id) LEFT JOIN " . DB_PREFIX . "specialist_to_store n2s ON (n.specialist_id = n2s.specialist_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

        if (isset($data['special'])) {
            $sql .= " AND n.special = '" . (int)$data['special'] . "'";
        }

        $sql .= " ORDER BY n.sort_order ASC, n.specialist_id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getSpecialistImages($specialist_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_image WHERE specialist_id = '" . (int)$specialist_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getSpecialistLayoutId($specialist_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "specialist_to_layout WHERE specialist_id = '" . (int)$specialist_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getTotalSpecialist() {
        $query = $this->db->query("SELECT COUNT(DISTINCT n.specialist_id) AS total FROM " . DB_PREFIX . "specialist n LEFT JOIN " . DB_PREFIX . "specialist_to_store n2s ON (n.specialist_id = n2s.specialist_id) WHERE n.status = '1' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

        return $query->row['total'];
    }
}