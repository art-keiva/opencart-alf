<?php
class ModelCmsPrice extends Model {
    public function getPrices() {
        $price_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "price");

        foreach ($query->rows as $result) {
            $price_data[$result['key']] = $result['value'];
        }

        return $price_data;
    }
}
