<?php
class ModelCmsPortfolio extends Model {
    public function getPortfolio($portfolio_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "portfolio WHERE portfolio_id = '" . (int)$portfolio_id . "' AND status = '1' ORDER BY sort_order ASC, title ASC");

        return $query->row;
    }

    public function getPortfolios(array $filter_data = []) {
        $sql = "SELECT * FROM " . DB_PREFIX . "portfolio WHERE status = '1'";

        if (isset($filter_data['filter_portfolio_id'])) {
            $sql .= " AND portfolio_id = '" . (int)$filter_data['filter_portfolio_id'] . "'";
        }

        $sql .= " ORDER BY sort_order ASC, LCASE(title) ASC";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getPortfolioImages(array $filter_data = []) {
        $sql = "SELECT * FROM " . DB_PREFIX . "portfolio_image";

        if (isset($filter_data['filter_portfolio_id']) or isset($filter_data['filter_special'])) {
            $sql .= " WHERE";
        }

        if (isset($filter_data['filter_portfolio_id'])) {
            $sql .= " portfolio_id = '" . (int)$filter_data['filter_portfolio_id'] . "'";
        }

        if (isset($filter_data['filter_special'])) {
            if (isset($filter_data['filter_portfolio_id'])) {
                $sql .= " AND";
            }
            $sql .= " special = '" . (int)$filter_data['filter_special'] . "'";
        }

        $sort_data = [
            'title',
            'sort_order',
        ];

        if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
            if ($filter_data['sort'] == 'title') {
                $sql .= " ORDER BY LCASE(" . $filter_data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $filter_data['sort'];
            }
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(title) DESC";
        } else {
            $sql .= " ASC, LCASE(title) ASC";
        }

        if (isset($filter_data['start']) || isset($filter_data['limit'])) {
            if ($filter_data['start'] < 0) {
                $filter_data['start'] = 0;
            }

            if ($filter_data['limit'] < 1) {
                $filter_data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalPortfolio() {
        $query = $this->db->query("SELECT COUNT(DISTINCT portfolio_id) AS total FROM " . DB_PREFIX . "portfolio WHERE status = '1'");

        return $query->row['total'];
    }

    public function getTotalPortfolioImages(array $filter_data = []) {
        $sql = "SELECT COUNT(DISTINCT portfolio_image_id) AS total FROM " . DB_PREFIX . "portfolio_image";

        if (isset($filter_data['filter_portfolio_id']) or isset($filter_data['filter_special'])) {
            $sql .= " WHERE";
        }

        if (isset($filter_data['filter_portfolio_id'])) {
            $sql .= " portfolio_id = '" . (int)$filter_data['filter_portfolio_id'] . "'";
        }

        if (isset($filter_data['filter_special'])) {
            if (isset($filter_data['filter_portfolio_id'])) {
                $sql .= " AND";
            }
            $sql .= " special = '" . (int)$filter_data['filter_special'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
}