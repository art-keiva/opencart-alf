<?php
class ModelCmsService extends Model {
	public function getService($service_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "service n LEFT JOIN " . DB_PREFIX . "service_description nd ON (n.service_id = nd.service_id) LEFT JOIN " . DB_PREFIX . "service_to_store n2s ON (n.service_id = n2s.service_id) WHERE n.service_id = '" . (int)$service_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");

		return $query->row;
	}

	public function getServices($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "service n LEFT JOIN " . DB_PREFIX . "service_description nd ON (n.service_id = nd.service_id) LEFT JOIN " . DB_PREFIX . "service_to_store n2s ON (n.service_id = n2s.service_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

        if (isset($data['special'])) {
            $sql .= " AND n.special = '" . (int)$data['special'] . "'";
        }
		
		$sql .= " ORDER BY n.sort_order ASC, n.service_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getServiceImages($service_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_image WHERE service_id = '" . (int)$service_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getServiceLayoutId($service_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_to_layout WHERE service_id = '" . (int)$service_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalService() {
		$query = $this->db->query("SELECT COUNT(DISTINCT n.service_id) AS total FROM " . DB_PREFIX . "service n LEFT JOIN " . DB_PREFIX . "service_to_store n2s ON (n.service_id = n2s.service_id) WHERE n.status = '1' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row['total'];
	}
}