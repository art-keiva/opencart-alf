<?php
class ModelCmsAction extends Model {
	public function getAction($action_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON (n.action_id = nd.action_id) LEFT JOIN " . DB_PREFIX . "action_to_store n2s ON (n.action_id = n2s.action_id) WHERE n.action_id = '" . (int)$action_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");

		return $query->row;
	}

	public function getActions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_description nd ON (n.action_id = nd.action_id) LEFT JOIN " . DB_PREFIX . "action_to_store n2s ON (n.action_id = n2s.action_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.date_added <= NOW() AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

		if (isset($data['special'])) {
			$sql .= " AND n.special = '" . (int)$data['special'] . "'";
		}
		
		// $sql .= " ORDER BY n.sort_order ASC, n.date_added DESC, n.action_id DESC";
		$sql .= " ORDER BY n.date_added DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getActionLayoutId($action_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "action_to_layout WHERE action_id = '" . (int)$action_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalAction() {
		$query = $this->db->query("SELECT COUNT(DISTINCT n.action_id) AS total FROM " . DB_PREFIX . "action n LEFT JOIN " . DB_PREFIX . "action_to_store n2s ON (n.action_id = n2s.action_id) WHERE n.status = '1' AND n.date_added <= NOW() AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row['total'];
	}
}