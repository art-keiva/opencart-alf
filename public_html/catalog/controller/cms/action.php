<?php
class ControllerCmsAction extends Controller {
	public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seoaction']['infoblock_seoaction_title']);
        $this->document->setDescription($infoblocks['seoaction']['infoblock_seoaction_description']);
        $this->document->setKeywords($infoblocks['seoaction']['infoblock_seoaction_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

		$this->load->language('cms/action');

		$this->load->model('cms/action');

		$this->load->model('tool/image');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['actions'] = array();

		if (isset($this->request->get['action_id'])) {
			$action_id = (int)$this->request->get['action_id'];
		} else {
			$action_id = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$action_info = $this->model_cms_action->getAction($action_id);

		if ($action_info) {
			$this->document->setTitle($action_info['meta_title']);
			$this->document->setDescription($action_info['meta_description']);
			$this->document->setKeywords($action_info['meta_keyword']);
			$this->document->addLink($this->url->link('cms/action', 'action_id=' . $this->request->get['action_id']), 'canonical');

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('cms/action')
			);

			$data['breadcrumbs'][] = array(
				'text' => $action_info['title'],
				'href' => $this->url->link('cms/action', 'action_id=' .  $action_id)
			);

			$data['action_id'] = $action_info['action_id'];
			$data['heading_title'] = $action_info['title'];

			// $data['date_added'] = date($this->language->get('date_format_short'), strtotime($action_info['date_added']));
			
			$data['date_added']= date($this->language->get('date_format_short'), strtotime($action_info['date_added']));
			$data['date_day']= date($this->language->get('date_format_day'), strtotime($action_info['date_added']));
			$data['date_month']= date($this->language->get('date_format_month'), strtotime($action_info['date_added']));
			$data['description'] = html_entity_decode($action_info['description'], ENT_QUOTES, 'UTF-8');
			
			if ($action_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($action_info['image'], 870, 570, 'max');
			} else {
				$data['thumb'] = '';
			}

			$limit = 15;

			$filter_data = array(
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			$action_total = $this->model_cms_action->getTotalAction();

			$results = $this->model_cms_action->getActions($filter_data);

			foreach ($results as $result) {
				
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 870, 570, 'max');
				} else {
					$image = '';
				}
				
				$data['actions'][] = array(
					'action_id' 	=> $result['action_id'],
					'name' 			=> $result['title'],
					'anons' 		=> $result['anons'],
					'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'thumb' 		=> $image,
					'href' 			=> $this->url->link('cms/action', 'action_id=' . $result['action_id'])
				);
			}

            $data = $this->controllers($data);

			$this->response->setOutput($this->load->view('cms/action', $data));
		} else {

			if (!$action_id AND empty($action_info)) {
                $this->document->setTitle($infoblocks['seoaction']['infoblock_seoaction_title']);
                $this->document->setDescription($infoblocks['seoaction']['infoblock_seoaction_description']);
                $this->document->setKeywords($infoblocks['seoaction']['infoblock_seoaction_keyword']);
				
				$data['heading_title'] = $this->language->get('text_action');
				$data['text_empty'] = $this->language->get('text_empty');
				
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('cms/action')
				);

				$limit = 15;

				$filter_data = array(
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);
				
				$action_total = $this->model_cms_action->getTotalAction();

				$results = $this->model_cms_action->getActions($filter_data);

				foreach ($results as $result) {
					
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], 870, 570, 'max');
					} else {
						$image = '';
					}
					
					$data['actions'][] = array(
						'action_id' 	=> $result['action_id'],
						'name' 			=> $result['title'],
						'anons' 		=> $result['anons'],
						'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
						'date_day' 	    => date($this->language->get('date_format_day'), strtotime($result['date_added'])),
						'date_month' 	=> date($this->language->get('date_format_month'), strtotime($result['date_added'])),
						'thumb' 		=> $image,
						'href' 			=> $this->url->link('cms/action', 'action_id=' . $result['action_id'])
					);
				}
				
				$pagination = new Pagination();
				$pagination->total = $action_total;
				$pagination->page = $page;
				$pagination->limit = $limit;
				$pagination->url = $this->url->link('cms/action', '&page={page}');

				$data['pagination'] = $pagination->render();

				$data['results'] = sprintf($this->language->get('text_pagination'), ($action_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($action_total - $limit)) ? $action_total : ((($page - 1) * $limit) + $limit), $action_total, ceil($action_total / $limit));

				// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
				if ($page == 1) {
					$this->document->addLink($this->url->link('cms/action'), 'canonical');
				} elseif ($page == 2) {
					$this->document->addLink($this->url->link('cms/action'), 'prev');
				} else {
					$this->document->addLink($this->url->link('cms/action', '&page='. ($page - 1), true), 'prev');
				}

				if ($limit && ceil($action_total / $limit) > $page) {
					$this->document->addLink($this->url->link('cms/action', '&page='. ($page + 1), true), 'next');
				}

                $data = $this->controllers($data);

				$this->response->setOutput($this->load->view('cms/action_list', $data));
			} else {
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_error'),
					'href' => $this->url->link('cms/action', 'information_id=' . $action_id)
				);

				$this->document->setTitle($this->language->get('text_error'));

				$data['heading_title'] = $this->language->get('text_error');

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');

				$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

                $data = $this->controllers($data);

				$this->response->setOutput($this->load->view('error/not_found', $data));
			}
		}
	}

    public function homeaction($parameters = [])
    {
        $this->load->language('cms/action');

        $this->load->model('cms/action');

        $this->load->model('tool/image');

        $data['homeactions'] = [];

        $limit = 15;

        $page = 1;

        $filter_data = [
            'special' => 1,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        ];

        $results = $this->model_cms_action->getActions($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 255, 170, 'max');
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 170, 'max');
            }

            $data['homeactions'][] = [
                'action_id' => $result['action_id'],
                'name' => $result['title'],
                'anons' => $result['anons'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_day' => date($this->language->get('date_format_day'), strtotime($result['date_added'])),
                'date_month' => date($this->language->get('date_format_month'), strtotime($result['date_added'])),
                'image' => $image,
                'href' => $this->url->link('cms/action', 'action_id=' . $result['action_id'])
            ];
        }

        $tag = 'action';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('cms/homeaction', $data);
    }

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}