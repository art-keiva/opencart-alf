<?php
class ControllerCmsSpecialist extends Controller {
    public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seospecialist']['infoblock_seospecialist_title']);
        $this->document->setDescription($infoblocks['seospecialist']['infoblock_seospecialist_description']);
        $this->document->setKeywords($infoblocks['seospecialist']['infoblock_seospecialist_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $this->load->language('cms/specialist');

        $this->load->model('cms/specialist');

        $this->load->model('tool/image');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['text_specialist_list'] = 'Список услуг';

        $data['specialists'] = array();

        if (isset($this->request->get['specialist_id'])) {
            $specialist_id = (int)$this->request->get['specialist_id'];
            $data['specialist_id'] = $specialist_id;
        } else {
            $specialist_id = 0;
            $data['specialist_id'] = $specialist_id;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $specialist_info = $this->model_cms_specialist->getSpecialist($specialist_id);

        if ($specialist_info) {
            $this->document->setTitle($specialist_info['meta_title']);
            $this->document->setDescription($specialist_info['meta_description']);
            $this->document->setKeywords($specialist_info['meta_keyword']);
            $this->document->addLink($this->url->link('cms/specialist', 'specialist_id=' . $this->request->get['specialist_id']), 'canonical');

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('cms/specialist')
            );

            $data['breadcrumbs'][] = array(
                'text' => $specialist_info['title'],
                'href' => $this->url->link('cms/specialist', 'specialist_id=' .  $specialist_id)
            );

            $data['heading_title'] = $specialist_info['title'];

            $data['anons'] = nl2br($specialist_info['anons']);

            if ($specialist_info['link']) {
                $link_parts = explode('/', (string)$specialist_info['link']);
                $link = array_pop($link_parts);
                $data['link_youtube'] = $link;
            }
            $data['link'] = nl2br($specialist_info['anons']);
            $data['description'] = html_entity_decode($specialist_info['description'], ENT_QUOTES, 'UTF-8');

            if ($specialist_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($specialist_info['image'], 480, 560, 'max');
            } else {
                $data['thumb'] = '';
            }

            $data['images'] = array();

            $results = $this->model_cms_specialist->getSpecialistImages($this->request->get['specialist_id']);

            foreach ($results as $result) {
                $data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], 700, 504, 'max'),
                    'thumb' => $this->model_tool_image->resize($result['image'], 455, 333, 'max')
                );
            }

            $limit = 15;
            $filter_data = array(
                'start'              => ($page - 1) * $limit,
                'limit'              => $limit
            );
            $results = $this->model_cms_specialist->getSpecialists($filter_data);

            foreach ($results as $result) {

                $data['specialists'][] = array(
                    'specialist_id' 	=> $result['specialist_id'],
                    'name' 			=> $result['title'],
                    'href' 			=> $this->url->link('cms/specialist', 'specialist_id=' . $result['specialist_id'])
                );
            }

            $data = $this->controllers($data);

            $this->response->setOutput($this->load->view('cms/specialist', $data));
        } else {

            if (!$specialist_id AND empty($specialist_info)) {
                $this->document->setTitle($infoblocks['seospecialist']['infoblock_seospecialist_title']);
                $this->document->setDescription($infoblocks['seospecialist']['infoblock_seospecialist_description']);
                $this->document->setKeywords($infoblocks['seospecialist']['infoblock_seospecialist_keyword']);

                $data['heading_title'] = $this->language->get('heading_title');
                $data['text_empty'] = $this->language->get('text_empty');

                $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('heading_title'),
                    'href' => $this->url->link('cms/specialist')
                );

                $limit = 15;

                $filter_data = array(
                    'start'              => ($page - 1) * $limit,
                    'limit'              => $limit
                );

                $specialist_total = $this->model_cms_specialist->getTotalSpecialist();

                $results = $this->model_cms_specialist->getSpecialists($filter_data);

                foreach ($results as $result) {

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], 446, 306, 'max');
                    } else {
                        $image = '';
                    }

                    $data['specialists'][] = array(
                        'specialist_id' 	=> $result['specialist_id'],
                        'name' 			=> $result['title'],
                         'anons' 		=> nl2br($result['anons']),
                        //'anons' 		=> utf8_substr($result['anons'], 0, 80),
                        'thumb' 		=> $image,
                        'href' 			=> $this->url->link('cms/specialist', 'specialist_id=' . $result['specialist_id'])
                    );
                }

                $pagination = new Pagination();
                $pagination->total = $specialist_total;
                $pagination->page = $page;
                $pagination->limit = $limit;
                $pagination->url = $this->url->link('cms/specialist', '&page={page}');

                $data['pagination'] = $pagination->render();

                $data['results'] = sprintf($this->language->get('text_pagination'), ($specialist_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($specialist_total - $limit)) ? $specialist_total : ((($page - 1) * $limit) + $limit), $specialist_total, ceil($specialist_total / $limit));

                // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
                if ($page == 1) {
                    $this->document->addLink($this->url->link('cms/specialist'), 'canonical');
                } elseif ($page == 2) {
                    $this->document->addLink($this->url->link('cms/specialist'), 'prev');
                } else {
                    $this->document->addLink($this->url->link('cms/specialist', '&page='. ($page - 1), true), 'prev');
                }

                if ($limit && ceil($specialist_total / $limit) > $page) {
                    $this->document->addLink($this->url->link('cms/specialist', '&page='. ($page + 1), true), 'next');
                }

                $data = $this->controllers($data);

                $this->response->setOutput($this->load->view('cms/specialist_list', $data));
            } else {
                $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('text_error'),
                    'href' => $this->url->link('cms/specialist', 'information_id=' . $specialist_id)
                );

                $this->document->setTitle($this->language->get('text_error'));

                $data['heading_title'] = $this->language->get('text_error');

                $data['text_error'] = $this->language->get('text_error');

                $data['button_continue'] = $this->language->get('button_continue');

                $data['continue'] = $this->url->link('common/home');

                $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

                $data = $this->controllers($data);

                $this->response->setOutput($this->load->view('error/not_found', $data));
            }
        }
    }

    public function homespecialist($parameters = [])
    {
        $this->load->language('cms/specialist');

        $this->load->model('cms/specialist');

        $this->load->model('tool/image');

        $data['homespecialists'] = [];

        $limit = 15;

        $page = 1;

        $filter_data = [
            'special' => 1,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        ];

        $results = $this->model_cms_specialist->getSpecialists($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 255, 210, 'max');
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 210, 'max');
            }

            $data['homespecialists'][] = [
                'specialist_id' => $result['specialist_id'],
                'name' 			=> $result['title'],
                'anons' 		=> nl2br($result['anons']),
                'image' 		=> $image,
                'href' 			=> $this->url->link('cms/specialist', 'specialist_id=' . $result['specialist_id'])
            ];
        }

        $tag = 'specialist';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                if ($key === 'infoblock_' . $tag . '_image' && !empty($value)) {
                    $value = $this->model_tool_image->resize($value, 1920, 500, 'max');
                }
                $data[$key] = $value;
            }
        }

        return $this->load->view('cms/homespecialist', $data);
    }

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}