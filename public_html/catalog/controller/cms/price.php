<?php
class ControllerCmsPrice extends Controller {
    public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seoprice']['infoblock_seoprice_title']);
        $this->document->setDescription($infoblocks['seoprice']['infoblock_seoprice_description']);
        $this->document->setKeywords($infoblocks['seoprice']['infoblock_seoprice_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $this->load->language('cms/price');

        $this->load->model('cms/price');

        $this->load->model('tool/image');

        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_price'),
            'href' => $this->url->link('cms/price')
        ];

        $data['heading_title'] = $this->language->get('text_price');

        $data['prices'] = [];

        $results = $this->model_cms_price->getPrices();

        foreach ($results as $key => $value) {
            $parts = explode('_', $key);

            if (!isset($data['prices']['price_' . $parts[1]])) {
                $data['prices']['price_' . $parts[1]] = [];
            }

            if ($parts[2] === 'description') {
                $value = html_entity_decode($value);
            }

            $data['prices']['price_' . $parts[1]][$parts[2]] = $value;
        }

        $data = $this->controllers($data);

        $this->response->setOutput($this->load->view('cms/price_list', $data));
    }

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}