<?php
class ControllerCmsNews extends Controller {
	public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seonews']['infoblock_seonews_title']);
        $this->document->setDescription($infoblocks['seonews']['infoblock_seonews_description']);
        $this->document->setKeywords($infoblocks['seonews']['infoblock_seonews_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

		$this->load->language('cms/news');

		$this->load->model('cms/news');

		$this->load->model('tool/image');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['text_news_list'] = 'Список новостей';

		$data['all_news'] = array();

		if (isset($this->request->get['news_id'])) {
			$news_id = (int)$this->request->get['news_id'];
			$data['news_id'] = $news_id;
		} else {
			$news_id = 0;
			$data['news_id'] = $news_id;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$news_info = $this->model_cms_news->getNews($news_id);

		if ($news_info) {
			$this->document->setTitle($news_info['meta_title']);
			$this->document->setDescription($news_info['meta_description']);
			$this->document->setKeywords($news_info['meta_keyword']);
			$this->document->addLink($this->url->link('cms/news', 'news_id=' . $this->request->get['news_id']), 'canonical');

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('cms/news')
			);

			$data['breadcrumbs'][] = array(
				'text' => $news_info['title'],
				'href' => $this->url->link('cms/news', 'news_id=' .  $news_id)
			);

			$data['heading_title'] = $news_info['title'];

			// $data['date_added'] = date($this->language->get('date_format_short'), strtotime($news_info['date_added']));
			
			$data['date_added']= date($this->language->get('date_format_short'), strtotime($news_info['date_added']));
			$data['date_day']= date($this->language->get('date_format_day'), strtotime($news_info['date_added']));
			$data['date_month']= date($this->language->get('date_format_month'), strtotime($news_info['date_added']));
			$data['description'] = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');
			
			if ($news_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($news_info['image'], 1110, 720, 'max');
			} else {
				$data['thumb'] = '';
			}

			$limit = 15;

			$filter_data = array(
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			$news_total = $this->model_cms_news->getTotalNews();

			$results = $this->model_cms_news->getAllNews($filter_data);

			foreach ($results as $result) {
				
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 870, 566, 'max');
				} else {
					$image = '';
				}
				
				$data['all_news'][] = array(
					'news_id' 		=> $result['news_id'],
					'name' 			=> $result['title'],
					'anons' 		=> $result['anons'],
					'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'thumb' 		=> $image,
					'href' 			=> $this->url->link('cms/news', 'news_id=' . $result['news_id'])
				);
			}

            $data = $this->controllers($data);

			$this->response->setOutput($this->load->view('cms/news', $data));
		} else {

			if (!$news_id AND empty($news_info)) {
                $this->document->setTitle($infoblocks['seonews']['infoblock_seonews_title']);
                $this->document->setDescription($infoblocks['seonews']['infoblock_seonews_description']);
                $this->document->setKeywords($infoblocks['seonews']['infoblock_seonews_keyword']);

				$data['heading_title'] = $this->language->get('text_news');
				$data['text_empty'] = $this->language->get('text_empty');
				
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('cms/news')
				);

				$limit = 15;

				$filter_data = array(
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);
				
				$news_total = $this->model_cms_news->getTotalNews();

				$results = $this->model_cms_news->getAllNews($filter_data);

				foreach ($results as $result) {
					
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], 870, 566, 'max');
					} else {
						$image = '';
					}
					
					$data['all_news'][] = array(
						'news_id' 		=> $result['news_id'],
						'name' 			=> $result['title'],
						'anons' 		=> $result['anons'],
						'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
						'date_day' 	    => date($this->language->get('date_format_day'), strtotime($result['date_added'])),
						'date_month' 	=> date($this->language->get('date_format_month'), strtotime($result['date_added'])),
						'thumb' 		=> $image,
						'href' 			=> $this->url->link('cms/news', 'news_id=' . $result['news_id'])
					);
				}
				
				$pagination = new Pagination();
				$pagination->total = $news_total;
				$pagination->page = $page;
				$pagination->limit = $limit;
				$pagination->url = $this->url->link('cms/news', '&page={page}');

				$data['pagination'] = $pagination->render();

				$data['results'] = sprintf($this->language->get('text_pagination'), ($news_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($news_total - $limit)) ? $news_total : ((($page - 1) * $limit) + $limit), $news_total, ceil($news_total / $limit));

				// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
				if ($page == 1) {
					$this->document->addLink($this->url->link('cms/news'), 'canonical');
				} elseif ($page == 2) {
					$this->document->addLink($this->url->link('cms/news'), 'prev');
				} else {
					$this->document->addLink($this->url->link('cms/news', '&page='. ($page - 1), true), 'prev');
				}

				if ($limit && ceil($news_total / $limit) > $page) {
					$this->document->addLink($this->url->link('cms/news', '&page='. ($page + 1), true), 'next');
				}

                $data = $this->controllers($data);


				$this->response->setOutput($this->load->view('cms/news_list', $data));
			} else {
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_error'),
					'href' => $this->url->link('cms/news', 'information_id=' . $news_id)
				);

				$this->document->setTitle($this->language->get('text_error'));

				$data['heading_title'] = $this->language->get('text_error');

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');

				$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

                $data = $this->controllers($data);

				$this->response->setOutput($this->load->view('error/not_found', $data));
			}
		}
	}

    public function homenews($parameters = [])
    {
        $this->load->language('cms/news');

        $this->load->model('cms/news');

        $this->load->model('tool/image');

        $data['homenewss'] = [];

        $limit = 15;

        $page = 1;

        $filter_data = [
            'special' => 1,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        ];

        $results = $this->model_cms_news->getAllNews($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 255, 170, 'max');
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 170, 'max');
            }

            $data['homenewss'][] = [
                'news_id' => $result['news_id'],
                'name' => $result['title'],
                'anons' => $result['anons'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_day' => date($this->language->get('date_format_day'), strtotime($result['date_added'])),
                'date_month' => date($this->language->get('date_format_month'), strtotime($result['date_added'])),
                'image' => $image,
                'href' => $this->url->link('cms/news', 'news_id=' . $result['news_id'])
            ];
        }

        $tag = 'news';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('cms/homenews', $data);
    }

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}