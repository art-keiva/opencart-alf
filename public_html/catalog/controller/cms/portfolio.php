<?php
class ControllerCmsPortfolio extends Controller {
    public const LIMIT = 12;

    public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seoportfolio']['infoblock_seoportfolio_title']);
        $this->document->setDescription($infoblocks['seoportfolio']['infoblock_seoportfolio_description']);
        $this->document->setKeywords($infoblocks['seoportfolio']['infoblock_seoportfolio_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $this->load->language('cms/portfolio');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_portfolio'),
            'href' => $this->url->link('cms/portfolio')
        );

        $data['heading_title'] = $this->language->get('text_portfolio');
        $data['text_empty'] = $this->language->get('text_empty');

        $filter_data = [
            'sort'               => 'sort_order',
            'order'              => 'ASC',
            'start'              => 0,
            'limit'              => self::LIMIT,
        ];

        $data = array_merge($filter_data, $data);

        $data['portfolios'] = $this->getPortfolios($filter_data);

        $data = $this->controllers($data);

        $this->response->setOutput($this->load->view('cms/portfolio_list', $data));
    }

    public function homeportfolio(array $parameters = [])
    {
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $filter_data = [
            'filter_special'     => 1,
            'sort'               => 'sort_order',
            'order'              => 'ASC',
            'start'              => 0,
            'limit'              => self::LIMIT,
        ];

        $data = array_merge($filter_data, $data);

        $data['portfolios'] = $this->getPortfolios($filter_data);

        $data = array_merge($this->getInfoblocksForSpecials($parameters), $data);

        return $this->load->view('cms/homeportfolio', $data);
    }

    public function load() {
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $this->load->language('cms/portfolio');
        $this->load->model('cms/portfolio');
        $this->load->model('tool/image');

        $filter_data = [
            'sort'               => 'sort_order',
            'order'              => 'ASC',
            'start'              => 0,
            'limit'              => self::LIMIT,
        ];

        if (isset($this->request->get['start'])) {
            $filter_data['start'] = abs((int)$this->request->get['start']);
        }

        if (isset($this->request->get['limit'])) {
            $filter_data['limit'] = abs((int)$this->request->get['limit']);
        }

        if (isset($this->request->get['special']) && !empty($this->request->get['special'])) {
            $filter_data['filter_special'] = abs((int)$this->request->get['special']);
        }

        $data['portfolio_id'] = $this->request->get['portfolio_id'];

        if (isset($this->request->get['portfolio_id']) && (int)$this->request->get['portfolio_id'] !== 0) {
            $filter_data['filter_portfolio_id'] = (int)$this->request->get['portfolio_id'];
        }

        $data = array_merge($filter_data, $data);

        $data['images'] = $this->getPortfolioImages($filter_data);

        $this->response->setOutput($this->load->view('cms/_load_portfolio_images', $data));
    }

    private function getPortfolios(array $filter_data = []) {
        $this->load->language('cms/portfolio');
        $this->load->model('cms/portfolio');
        $this->load->model('tool/image');

        $firstTab = [];
        $firstTab[] = [
            'portfolio_id'  => 0,
            'title'         => $this->language->get('text_all'),
            'total'         => $this->model_cms_portfolio->getTotalPortfolioImages($filter_data),
            'images'        => $this->getPortfolioImages($filter_data),
        ];

        $otherTabs = [];
        $portfolios = $this->model_cms_portfolio->getPortfolios($filter_data);
        foreach ($portfolios as $portfolio) {
            $filter_data['filter_portfolio_id'] = $portfolio['portfolio_id'];

            $otherTabs[] = [
                'portfolio_id'  => $portfolio['portfolio_id'],
                'title'         => $portfolio['title'],
                'total'         => $this->model_cms_portfolio->getTotalPortfolioImages($filter_data),
                'images'        => $this->getPortfolioImages($filter_data),
            ];
        }

        return array_merge($firstTab, $otherTabs);
    }

    private function getPortfolioImages(array $filter_data = []) {
        $images = [];

        $results = $this->model_cms_portfolio->getPortfolioImages($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 800, 600);
                $thumb = $this->model_tool_image->resize($result['image'], 400, 340, 'max');
            } else {
                $image = '';
                $thumb = '';
            }

            $images[] = [
                'portfolio_image_id' => $result['portfolio_image_id'],
                'portfolio_id' => $result['portfolio_id'],
                'title' => $result['title'],
                'sort_order' => $result['sort_order'],
                'special' => $result['special'],
                'image' => $image,
                'thumb' => $thumb,
            ];
        }

        return $images;
    }

    private function getInfoblocksForSpecials(array $parameters = [])
    {
        $data = [];

        $tag = 'portfolio';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                if ($key === 'infoblock_' . $tag . '_image' && !empty($value)) {
                    $value = $this->model_tool_image->resize($value, 1920, 500, 'max');
                }
                $data[$key] = $value;
            }
        }

        return $data;
    }

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}