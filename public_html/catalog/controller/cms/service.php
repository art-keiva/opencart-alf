<?php
class ControllerCmsService extends Controller {
	public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seoservice']['infoblock_seoservice_title']);
        $this->document->setDescription($infoblocks['seoservice']['infoblock_seoservice_description']);
        $this->document->setKeywords($infoblocks['seoservice']['infoblock_seoservice_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');
		
		$this->load->language('cms/service');

		$this->load->model('cms/service');

		$this->load->model('tool/image');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['text_service_list'] = 'Список услуг';

		$data['services'] = array();

		if (isset($this->request->get['service_id'])) {
			$service_id = (int)$this->request->get['service_id'];
			$data['service_id'] = $service_id;
		} else {
			$service_id = 0;
			$data['service_id'] = $service_id;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$service_info = $this->model_cms_service->getService($service_id);

		if ($service_info) {
			$this->document->setTitle($service_info['meta_title']);
			$this->document->setDescription($service_info['meta_description']);
			$this->document->setKeywords($service_info['meta_keyword']);
			$this->document->addLink($this->url->link('cms/service', 'service_id=' . $this->request->get['service_id']), 'canonical');

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('cms/service')
			);

			$data['breadcrumbs'][] = array(
				'text' => $service_info['title'],
				'href' => $this->url->link('cms/service', 'service_id=' .  $service_id)
			);

			$data['heading_title'] = $service_info['title'];

			$data['description'] = html_entity_decode($service_info['description'], ENT_QUOTES, 'UTF-8');
			
			if ($service_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($service_info['image'], 1043, 507, 'max');
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_cms_service->getServiceImages($this->request->get['service_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], 455, 333, 'max'),
					'thumb' => $this->model_tool_image->resize($result['image'], 455, 333, 'max')
				);
			}
			
			$limit = 15;
			$filter_data = array(
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			$results = $this->model_cms_service->getServices($filter_data);

			foreach ($results as $result) {
					
				$data['services'][] = array(
					'service_id' 	=> $result['service_id'],
					'name' 			=> $result['title'],
					'href' 			=> $this->url->link('cms/service', 'service_id=' . $result['service_id'])
				);
			}

            $data = $this->controllers($data);

			$this->response->setOutput($this->load->view('cms/service', $data));
		} else {

			if (!$service_id AND empty($service_info)) {
                $this->document->setTitle($infoblocks['seoservice']['infoblock_seoservice_title']);
                $this->document->setDescription($infoblocks['seoservice']['infoblock_seoservice_description']);
                $this->document->setKeywords($infoblocks['seoservice']['infoblock_seoservice_keyword']);
				
				$data['heading_title'] = $this->language->get('heading_title');
				$data['text_empty'] = $this->language->get('text_empty');
				
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('heading_title'),
					'href' => $this->url->link('cms/service')
				);

				$limit = 15;

				$filter_data = array(
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);
				
				$service_total = $this->model_cms_service->getTotalService();

				$results = $this->model_cms_service->getServices($filter_data);

				foreach ($results as $result) {
					
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], 446, 306, 'max');
					} else {
						$image = '';
					}
					
					$data['services'][] = array(
						'service_id' 	=> $result['service_id'],
						'name' 			=> $result['title'],
						// 'anons' 		=> $result['anons'],
						'anons' 		=> utf8_substr($result['anons'], 0, 80),
						'thumb' 		=> $image,
						'href' 			=> $this->url->link('cms/service', 'service_id=' . $result['service_id'])
					);
				}
				
				$pagination = new Pagination();
				$pagination->total = $service_total;
				$pagination->page = $page;
				$pagination->limit = $limit;
				$pagination->url = $this->url->link('cms/service', '&page={page}');

				$data['pagination'] = $pagination->render();

				$data['results'] = sprintf($this->language->get('text_pagination'), ($service_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($service_total - $limit)) ? $service_total : ((($page - 1) * $limit) + $limit), $service_total, ceil($service_total / $limit));

				// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
				if ($page == 1) {
					$this->document->addLink($this->url->link('cms/service'), 'canonical');
				} elseif ($page == 2) {
					$this->document->addLink($this->url->link('cms/service'), 'prev');
				} else {
					$this->document->addLink($this->url->link('cms/service', '&page='. ($page - 1), true), 'prev');
				}

				if ($limit && ceil($service_total / $limit) > $page) {
					$this->document->addLink($this->url->link('cms/service', '&page='. ($page + 1), true), 'next');
				}

                $data = $this->controllers($data);

				$this->response->setOutput($this->load->view('cms/service_list', $data));
			} else {
				$data['breadcrumbs'][] = array(
					'text' => $this->language->get('text_error'),
					'href' => $this->url->link('cms/service', 'information_id=' . $service_id)
				);

				$this->document->setTitle($this->language->get('text_error'));

				$data['heading_title'] = $this->language->get('text_error');

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');

				$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

                $data = $this->controllers($data);

				$this->response->setOutput($this->load->view('error/not_found', $data));
			}
		}
	}

    public function homeservice($parameters = [])
    {
        $this->load->language('cms/service');

        $this->load->model('cms/service');

        $this->load->model('tool/image');

        $data['homeservices'] = [];

        $limit = 15;

        $page = 1;

        $filter_data = [
            'special' => 1,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        ];

        $results = $this->model_cms_service->getServices($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 255, 210, 'max');
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 210, 'max');
            }

            $data['homeservices'][] = [
                'service_id' 	=> $result['service_id'],
                'name' 			=> $result['title'],
                'anons' 		=> $result['anons'],
                'image' 		=> $image,
                'href' 			=> $this->url->link('cms/service', 'service_id=' . $result['service_id'])
            ];
        }

        $tag = 'service';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                if ($key === 'infoblock_' . $tag . '_image' && !empty($value)) {
                    $value = $this->model_tool_image->resize($value, 1920, 490, 'max');
                }
                $data[$key] = $value;
            }
        }

        return $this->load->view('cms/homeservice', $data);
	}

    private function controllers(array $data = [])
    {
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        return $data;
    }
}