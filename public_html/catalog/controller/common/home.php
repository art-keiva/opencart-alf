<?php
class ControllerCommonHome extends Controller {
	public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seohome']['infoblock_seohome_title'] ?? null);
        $this->document->setDescription($infoblocks['seohome']['infoblock_seohome_description'] ?? null);
        $this->document->setKeywords($infoblocks['seohome']['infoblock_seohome_keyword'] ?? null);
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $infoblocks, 'categories' => $categories]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $infoblocks, 'categories' => $categories]);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

        $results = $this->load->controller('infoblock/infoblock/handleInfo', ['infoblocks' => $infoblocks]);
        $data = array_merge($results, $data);

		$data['homeslider'] = $this->load->controller('infoblock/homeslider', ['infoblocks' => $infoblocks]);
		$data['homeadvantage'] = $this->load->controller('infoblock/homeadvantage', ['infoblocks' => $infoblocks]);
		$data['homebanner1'] = $this->load->controller('infoblock/homebanner1', ['infoblocks' => $infoblocks]);
		$data['homebanner2'] = $this->load->controller('infoblock/homebanner2', ['infoblocks' => $infoblocks]);
		$data['homebanner3'] = $this->load->controller('infoblock/homebanner3', ['infoblocks' => $infoblocks]);
		$data['homebanner4'] = $this->load->controller('infoblock/homebanner4', ['infoblocks' => $infoblocks]);
		$data['homebanner5'] = $this->load->controller('infoblock/homebanner5', ['infoblocks' => $infoblocks]);
		$data['homebanner6'] = $this->load->controller('infoblock/homebanner6', ['infoblocks' => $infoblocks]);
		$data['homefaq'] = $this->load->controller('infoblock/homefaq', ['infoblocks' => $infoblocks]);
		$data['hometestimonial'] = $this->load->controller('infoblock/hometestimonial', ['infoblocks' => $infoblocks]);
		$data['homeaction'] = $this->load->controller('cms/action/homeaction', ['infoblocks' => $infoblocks]);
		$data['homenews'] = $this->load->controller('cms/news/homenews', ['infoblocks' => $infoblocks]);
		$data['homeservice'] = $this->load->controller('cms/service/homeservice', ['infoblocks' => $infoblocks]);
		//$data['homespecial'] = $this->load->controller('product/product/special', ['infoblocks' => $infoblocks]);
		$data['homespecial'] = $this->load->controller('product/product/special_tabs', ['infoblocks' => $infoblocks]);
		$data['homefeatured'] = $this->load->controller('product/product/featured', ['infoblocks' => $infoblocks]);
		$data['homeportfolio'] = $this->load->controller('cms/portfolio/homeportfolio', ['infoblocks' => $infoblocks]);
		$data['homespecialist'] = $this->load->controller('cms/specialist/homespecialist', ['infoblocks' => $infoblocks]);

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
