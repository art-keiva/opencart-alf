<?php
class ControllerCommonFooter extends Controller {
	public function index($parameters = []) {
        $data['version_html'] = HTML_VERSION;

		$this->load->language('common/footer');

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $data['base'] = $server;

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $data['name'] = $this->config->get('config_name');
        $data['address'] = $this->config->get('config_address');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['telephone2'] = $this->config->get('config_telephone2');
        $data['telephone3'] = $this->config->get('config_telephone3');
        $data['email'] = $this->config->get('config_email');
        $data['comment'] = $this->config->get('config_comment');
        $data['open'] = html_entity_decode($this->config->get('config_open')) ;

        if (isset($parameters['infoblocks']['contact'])) {
            foreach ($parameters['infoblocks']['contact'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (isset($parameters['infoblocks']['social'])) {
            foreach ($parameters['infoblocks']['social'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['categories'] = [];

        if (isset($parameters['categories'])) {
            $data['categories'] = $parameters['categories'];
        }

        $data['menu'] = $this->load->controller('common/menu', [
            'position' => '_footer',
            'categories' => $data['categories']
        ]);

		//$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
		$data['powered'] = sprintf($this->language->get('text_powered'), date('Y', time()));

		$data['scripts'] = $this->document->getScripts('footer');
		
		return $this->load->view('common/footer', $data);
	}
}
