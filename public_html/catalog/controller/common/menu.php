<?php
class ControllerCommonMenu extends Controller {
    public function index($parameters = []) {
        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string)$this->request->get['path']);
        } else {
            $parts = array();
        }

        $data['ishome'] = false;
        if (!isset($this->request->get['route']) || $this->request->get['route'] == 'common/home'){
            $data['ishome'] = true;
        }

        if (isset($parts[1])) {
            $data['category_id'] = $parts[1];
        } else {
            $data['category_id'] = 0;
        }

        if (isset($parts[2])) {
            $data['child_id'] = $parts[2];
        } else {
            $data['child_id'] = 0;
        }

        $data['categories'] = [];

        if (isset($parameters['categories'])) {
            $data['categories'] = $parameters['categories'];
        }

        //// SERVICES
        //$this->load->model('cms/service');
        //
        //$data['services'] = [];
        //
        //$limit = 8;
        //
        //$filter_data = array(
        //    'start'              => 0 * $limit,
        //    'limit'              => $limit
        //);
        //
        //$results = $this->model_cms_service->getServices($filter_data);
        //
        //foreach ($results as $result) {
        //    $data['services'][] = array(
        //        'service_id' 	=> $result['service_id'],
        //        'name' 			=> $result['title'],
        //        'href' 			=> $this->url->link('cms/service', 'service_id=' . $result['service_id'])
        //    );
        //}

        $this->load->language('common/menu');

        $active_class = 'active';
        $empty_link = 'javascript:;';

        $data['main_menu'] = array(
            'home' 		=> array(
                'name' 		=> $this->language->get('text_home'),
                'active' 	=> false,
                'href' 		=> $this->url->link('common/home'),
            ),
            'shop' 		=> array(
                'name' 		=> $this->language->get('text_catalog'),
                'active' 	=> false,
                'href' 		=> $this->url->link('product/category'),
                'child' 	=> $data['categories'],
            ),
            // 'action' 		=> array(
            // 	'name' 		=> 'Акции',
            // 	'active' 	=> false,
            // 	'href' 		=> $this->url->link('cms/action', '', true),
            // ),
            'service' 	=> array(
                'name' 		=> 'Услуги',
                'active' 	=> false,
                'href' 		=> $this->url->link('cms/service', '', true),
            ),
            'about' 		=> array(
                'name' 		=> $this->language->get('text_about'),
                'active' 	=> false,
                'href' 		=> $this->url->link('information/information', 'information_id=4'),
                'child' 	=> array(
                    // 'delivery' 	=> array(
                    // 	'name' 		=> 'Доставка груза',
                    // 	'active' 	=> false,
                    // 	'href' 		=> $this->url->link('information/information', 'information_id=6'),
                    // ),
                    'faq' 		=> array(
                        'name' 		=> $this->language->get('text_faq'),
                        'active' 	=> false,
                        'href' 		=> $this->url->link('information/faq', '', true),
                    ),
                    'portfolio' => array(
                        'name' 		=> $this->language->get('text_portfolio'),
                        'active' 	=> false,
                        'href' 		=> $this->url->link('cms/portfolio', '', true),
                    ),
                    'specialist' => array(
                        'name' 		=> $this->language->get('text_specialist'),
                        'active' 	=> false,
                        'href' 		=> $this->url->link('cms/specialist', '', true),
                    ),
                    'price' => array(
                        'name' 		=> $this->language->get('text_price'),
                        'active' 	=> false,
                        'href' 		=> $this->url->link('cms/price', '', true),
                    ),
                ),
            ),
            'news' 		=> array(
                'name' 		=> $this->language->get('text_news'),
                'active' 	=> false,
                'href' 		=> $this->url->link('cms/news', '', true),
                // 'child' 	=> array(
                // 	'faq' 		=> array(
                // 		'name' 		=> 'Вопросы',
                // 		'active' 	=> false,
                // 		'href' 		=> $this->url->link('information/faq', '', true),
                // 	),
                // ),
            ),
            // 'docs' 		=> array(
            // 	'name' 		=> 'Документы',
            // 	'active' 	=> false,
            // 	'href' 		=> $this->url->link('cms/doc', '', true),
            // ),
            'contact' 	=> array(
                'name' 		=> $this->language->get('text_contact'),
                'active' 	=> false,
                'href' 		=> $this->url->link('information/contact'),
            ),
        );

        if (isset($this->request->get['route'])){
            if ($this->request->get['route'] == 'common/home'){
                $data['main_menu']['home']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'product/category' || $this->request->get['route'] == 'product/product'){
                $data['main_menu']['shop']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'cms/service'){
                $data['main_menu']['service']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'cms/doc'){
                $data['main_menu']['docs']['active'] = $active_class;
            }
            if (($this->request->get['route'] == 'information/information' && $this->request->get['information_id'] == 4) || $this->request->get['route'] == 'information/faq'){
                $data['main_menu']['about']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'cms/news'){
                $data['main_menu']['news']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'cms/action'){
                $data['main_menu']['action']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'cms/portfolio'){
                $data['main_menu']['portfolio']['active'] = $active_class;
            }
            if ($this->request->get['route'] == 'information/contact'){
                $data['main_menu']['contact']['active'] = $active_class;
            }
        } else {
            $data['main_menu']['home']['active'] = $active_class;
        }

        $position = '';

        if (isset($parameters['position'])) {
            $position = $parameters['position'];
        }

        return $this->load->view('common/menu' . $position, $data);
    }
}
