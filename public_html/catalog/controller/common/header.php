<?php
class ControllerCommonHeader extends Controller {
    public function index($parameters = []) {
        $data['version_html'] = HTML_VERSION;

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');

		$data['name'] = $this->config->get('config_name');
        $data['theme_dir'] = $this->config->get('theme_default_directory');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');
			//$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			$data['total_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
		} else {
			//$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			$data['total_wishlist'] = (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0);
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		
		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');

        $data['address'] = $this->config->get('config_address');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['telephone2'] = $this->config->get('config_telephone2');
        $data['telephone3'] = $this->config->get('config_telephone3');
        $data['email'] = $this->config->get('config_email');
        $data['open'] = html_entity_decode($this->config->get('config_open'));

        if (isset($parameters['infoblocks']['contact'])) {
            foreach ($parameters['infoblocks']['contact'] as $key => $value) {
                $data[$key] = $value;
            }
        }

        $data['class'] = 'page-';
        if (isset($this->request->get['route'])){
            $data['class'] .= str_replace('/', '-', $this->request->get['route']);
            $data['isHome'] = false;
        } else {
            $data['class'] .= 'home';
            $data['isHome'] = true;
        }

        $data['categories'] = [];

        if (isset($parameters['categories'])) {
            $data['categories'] = $parameters['categories'];
        }

        //$data['categories'] = $this->load->controller('common/categories');
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu', [
            'position' => '_header',
		    'categories' => $data['categories']
        ]);

		return $this->load->view('common/header', $data);
	}
}
