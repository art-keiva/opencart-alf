<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $infoblocks, 'categories' => $categories]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $infoblocks, 'categories' => $categories]);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_catalog'),
			'href' => $this->url->link('product/category')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			$this->document->setTitle($product_info['meta_title']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['name'];

			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);

			$data['product_id'] = (int)$this->request->get['product_id'];
            $data['anons'] = nl2br($product_info['anons']);
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            $data['sku'] = $product_info['sku'];

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_height'))
				);
			}

			if ((float) $product_info['price']) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['price'] = false;
			}

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			// Captcha
			if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			// Filters
            $this->load->model('catalog/filter');

            $data['product_filters'] = [];

            $product_filters = $this->model_catalog_product->getProductFilters($this->request->get['product_id']);

            $data['product_filters'] = $product_filters;

            $data['filter_groups'] = array();

            $filter_data = [
                'sort' => 'fg.sort_order',
                'order' => 'ASC',
            ];

            $filter_groups = $this->model_catalog_filter->getFilterGroups($filter_data);

            foreach ($filter_groups as $filter_group) {
                $filters = [];

                $results = $this->model_catalog_filter->getFilters(['filter_group' => $filter_group['filter_group_id']]);

                foreach ($results as $result) {
                    if (in_array($result['filter_id'], $product_filters)) {
                        $filters[] = $result;
                    }
                }

                $data['filter_groups'][] = [
                    'filter_group_id' => $filter_group['filter_group_id'],
                    'name' => $filter_group['name'],
                    'filters' => $filters,
                ];
            }

			$data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

            if (isset($this->request->get['quickview'])) {
                $this->response->setOutput($this->load->view('product/product_quickview', $data));
                return;
            }

			$this->response->setOutput($this->load->view('product/product', $data));
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

    public function special(array $parameters = []) {
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $start = 0;
        $limit = 2;
        $data['limit'] = $limit;

        if (isset($this->request->get['length'])) {
            $start = abs((int)$this->request->get['length']);
        }

        $filter_data = [
            'filter_is_special' => 1,
            'sort'              => 'sort',
            'order'             => 'ASC',
            'start'             => $start,
            'limit'             => $limit,
        ];

        $specials = $this->getSpecials([
            'filter_data' => $filter_data,
        ]);

        $data['specials'] = $specials;

        if (isset($this->request->get['length'])) {
            return $this->response->setOutput($this->load->view('product/_products_specials', $data));
        }

        $data = array_merge($this->getInfoblocksForSpecials($parameters), $data);

        return $this->load->view('product/_specials', $data);
    }

    public function special_tabs(array $parameters = []) {
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $category_id = 0;
        $start = 0;
        $limit = 2;

        if (isset($this->request->get['length']) && isset($this->request->get['category_id'])) {
            $category_id = abs((int)$this->request->get['category_id']);
            $start = abs((int)$this->request->get['length']);
        }

        $filter_data = [
            'filter_is_special'     => 1,
            'filter_category_id'    => $category_id,
            'sort'                  => 'sort',
            'order'                 => 'ASC',
            //'start'                 => $start,
            //'limit'                 => $limit,
        ];

        $specials = $this->getSpecialsTabs([
            'filter_data' => $filter_data,
        ]);

        $data['specials'] = $specials;
        $data['category_id'] = $category_id;
        $data['start'] = $start;
        $data['limit'] = $limit;

        if (isset($this->request->get['length'])) {
            return $this->response->setOutput($this->load->view('product/_products_specials_tabs', $data));
        }

        $data = array_merge($this->getInfoblocksForSpecials($parameters), $data);

        return $this->load->view('product/_special_tabs', $data);
    }

    public function featured(array $parameters = []) {
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $featureds = $this->getFeatureds();

        $data['featureds'] = $featureds;

        if (isset($this->request->get['length'])) {
            return $this->response->setOutput($this->load->view('product/_featureds', $data));
        }

        $data = array_merge($this->getInfoblocksForFeatureds($parameters), $data);

        return $this->load->view('product/_featureds', $data);
    }

    private function getSpecials(array $data = []) {
        if (!isset($data['filter_data'])) {
            throw new \Exception('Too few parameters');
        }

        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $products = [];

        $product_total = $this->model_catalog_product->getTotalProducts($data['filter_data']);
        $results = $this->model_catalog_product->getProducts($data['filter_data']);

        foreach ($results as $result) {
            $product_info = $result;
            if ($product_info['image']) {
                $image = $this->model_tool_image->resize($product_info['image'], 255, 200);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 200);
            }

            if ((float)$product_info['price']) {
                $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            $products[] = [
                'product_id'  => $product_info['product_id'],
                'thumb'       => $image,
                'name'        => $product_info['name'],
                'price'       => $price,
                'quickview'   => $this->url->link('product/product', 'product_id=' . $product_info['product_id'] . '&quickview=1'),
                'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
            ];
        }

        return [
            'products' => $products,
            'total' => $product_total,
        ];
    }

    private function getSpecialsTabs(array $data = []) {
        if (!isset($data['filter_data'])) {
            throw new \Exception('Too few parameters');
        }

        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $categories = [];
        $products = [];

        //unset($data['filter_data']['start']);
        //unset($data['filter_data']['limit']);
        $categories = [];
        $products = [];

        $product_total = $this->model_catalog_product->getTotalProducts($data['filter_data']);
        $results = $this->model_catalog_product->getProducts($data['filter_data']);

        foreach ($results as $result) {
            $product_info = $result;
            if ($product_info['image']) {
                $image = $this->model_tool_image->resize($product_info['image'], 255, 200);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 255, 200);
            }

            if ((float)$product_info['price']) {
                $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            $product = [
                'product_id'  => $product_info['product_id'],
                'thumb'       => $image,
                'name'        => $product_info['name'],
                'price'       => $price,
                'quickview'   => $this->url->link('product/product', 'product_id=' . $product_info['product_id'] . '&quickview=1'),
                'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
            ];

            $products[] = $product;

            $category_id = $this->model_catalog_product->getRootCategoryIdByProductId($product_info['product_id']);

            if ($category_id) {
                if (!isset($categories[$category_id])) {
                    $categories[$category_id] = $this->model_catalog_category->getCategory($category_id);
                    $categories[$category_id]['products'] = [];
                    $categories[$category_id]['total'] = 0;
                }

                $categories[$category_id]['products'][] = $product;
                $categories[$category_id]['total']++;
            }
        }

        return [
            'categories' => $categories,
            'products' => $products,
            'total' => $product_total,
        ];
    }

    private function getFeatureds() {
        $this->load->language('extension/module/featured');
        $this->load->model('setting/module');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $products = [];

        $results = $this->model_setting_module->getModuleByCode('featured');

        foreach ($results as $result) {
            $setting = json_decode($result['setting'], true);
            $featued_products = array();

            if (!$setting['limit']) {
                $setting['limit'] = 4;
            }

            if (!empty($setting['product']) AND $setting['status'] == 1) {
                $products = array_slice($setting['product'], 0, (int)$setting['limit']);

                foreach ($products as $product_id) {
                    $product_info = $this->model_catalog_product->getProduct($product_id);

                    if ($product_info) {
                        if ($product_info['image']) {
                            $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                        }

                        $price = false;

                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            if ($product_info['price'] > 0) {
                                if ((float)$product_info['price']) {
                                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                                }
                            }
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = $product_info['rating'];
                        } else {
                            $rating = false;
                        }

                        $featued_products[] = array(
                            'product_id'  => $product_info['product_id'],
                            'thumb'       => $image,
                            'name'        => $product_info['name'],
                            'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                            'price'       => $price,
                            'rating'      => $rating,
                            'quickview'   => $this->url->link('product/product', 'product_id=' . $product_info['product_id'] . '&quickview=1'),
                            'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                        );
                    }
                }
            }

            $data['featureds'][] = array(
                'module_id'   	=> $result['module_id'],
                'limit'   		=> $setting['limit'],
                'total'   		=> count($setting['product']),
                'name'        	=> $setting['name'],
                'products'    	=> $featued_products
            );
        }

        return $data['featureds'];
    }

    // Temporarily function for featured module
    public function products()
    {
        $data['theme_dir'] = $this->config->get('theme_default_directory');
        $data['i'] = 1;

        if (isset($this->request->get['group'])) {
            $data['i'] = $this->request->get['group'];
        }

        $data['products'] = [];

        // echo '<pre>'; var_dump($this->request->get); echo '</pre>';
        // echo '<pre>'; var_dump($this->request->get['module_id']); echo '</pre>';

        if (isset($this->request->get['module_id']) && isset($this->request->get['circle'])) {

            // featured
            $this->load->model('setting/module');

            $data['featureds'] = array();

            $result = $this->model_setting_module->getModule2($this->request->get['module_id']);

            // echo '<pre>'; var_dump($result); echo '</pre>';

            if ($result) {
                $setting = json_decode($result['setting'], true);


                // echo '<pre>'; var_dump($setting); echo '</pre>';

                $this->load->language('extension/module/featured');

                $this->load->model('catalog/product');

                $this->load->model('tool/image');

                $featued_products = array();

                if (!$setting['limit']) {
                    $setting['limit'] = 4;
                }

                if (!empty($setting['product']) AND $setting['status'] == 1) {
                    $start = (int)$this->request->get['circle'] * (int)$setting['limit'];

                    $products = array_slice($setting['product'], $start, (int)$setting['limit']);

                    foreach ($products as $product_id) {

                        $product_info = $this->model_catalog_product->getProduct($product_id);

                        if ($product_info) {
                            if ($product_info['image']) {
                                $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                            }

                            $price = false;

                            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                                if ($product_info['price'] > 0) {
                                    if ((float)$product_info['price']) {
                                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                                    }
                                }
                            }

                            if ($this->config->get('config_review_status')) {
                                $rating = $product_info['rating'];
                            } else {
                                $rating = false;
                            }

                            $data['products'][] = array(
                                'product_id'  => $product_info['product_id'],
                                'thumb'       => $image,
                                'name'        => $product_info['name'],
                                'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                                'price'       => $price,
                                'rating'      => $rating,
                                'quickview'   => $this->url->link('product/product', 'product_id=' . $product_info['product_id'] . '&quickview=1'),
                                'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
                            );
                        }
                    }
                }
            }
        }

        $this->response->setOutput($this->load->view('product/_products_featureds', $data));
        // return $this->load->view('extension/module/featured/featured_product', $data);
    }

    private function getInfoblocksForSpecials(array $parameters = [])
    {
        $data = [];

        $tag = 'special';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    private function getInfoblocksForFeatureds(array $parameters = [])
    {
        $data = [];

        $tag = 'featured';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $data;
    }
}
