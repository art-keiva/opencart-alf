<?php
class ControllerProductCategories extends Controller {
    public function index() {
        $this->load->language('product/categories');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            // Level 2
            $children_data = array();

            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {
                $filter_data = array(
                    'filter_category_id'  => $child['category_id'],
                    'filter_sub_category' => true
                );

                $children_data[] = array(
                    'name'  => $child['name'],
                    'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                );
            }

            if ($category['image']) {
                $thumb = $this->model_tool_image->resize(
                    $category['image'],
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_width'),
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_category_height')
                );
            } else {
                $thumb = $this->model_tool_image->resize(
                    'placeholder.png',
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'),
                    $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height')
                );
            }

            // Level 1
            $data['categories'][] = array(
                'name'     => $category['name'],
                'thumb'    => $thumb,
                'children' => $children_data,
                'column'   => $category['column'] ? $category['column'] : 1,
                'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
            );
        }

        return $data['categories'];
    }
}
