<?php
class ControllerCheckoutCheckout extends Controller {
    private $error = array();
    private $emails = array();

    public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        //return $this->load->controller('checkout/checkout2');

        $this->load->language('checkout/checkout');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            if (!$this->cart->hasProducts()) {
                $this->response->redirect($this->url->link('checkout/cart'));
            }

            $send = $this->sendEmails($this->request->post);

            if ($send) {
                $this->cart->clear();

                $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('checkout/checkout'));
            } else {
                $this->session->data['fail'] = sprintf($this->language->get('text_fail'), $this->config->get('config_email'));

                $this->response->redirect($this->url->link('checkout/checkout'));
            }
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';

            // Validate cart has products and has stock.
            if (!$this->cart->hasProducts()) {
                $this->response->redirect($this->url->link('checkout/cart'));
            }
        }

        if (isset($this->session->data['fail'])) {
            $data['fail'] = $this->session->data['fail'];

            unset($this->session->data['fail']);
        } else {
            $data['fail'] = '';
        }

        $data['error'] = $this->error;

        $formFields = [
            'shipping',
            'payment',
            'name',
            'email',
            'phone',
            'address',
            'city',
            'code',
            'country_id',
            'zone_id',
            'comment',
            'agree',
            'captcha',
        ];

        foreach ($formFields as $key => $fieldName) {
            if (isset($this->request->post[$fieldName])) {
                $data[$fieldName] = $this->request->post[$fieldName];
            } else {
                $data[$fieldName] = '';
            }
        }

        // TERMS AGREE
        if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if ($information_info) {
                $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information', 'information_id=' . $this->config->get('config_checkout_id'), true), $information_info['title'], $information_info['title']);
            } else {
                $data['text_agree'] = '';
            }
        } else {
            $data['text_agree'] = '';
        }

        if (isset($this->session->data['agree'])) {
            $data['agree'] = $this->session->data['agree'];
        } else {
            $data['agree'] = '';
        }

        // CAPTCHA
        $data['recaptcha'] = '';

        if (defined('reCAPTCHA_sitekey')) {
            $this->document->addScript('https://www.google.com/recaptcha/api.js');
            $data['recaptcha'] = reCAPTCHA_sitekey;
        }

        // COUNTRY & ZONE
        if (isset($this->request->post['country_id'])) {
            $data['country_id'] = $this->request->post['country_id'];
        } else {
            $data['country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['zone_id'])) {
            $data['zone_id'] = $this->request->post['zone_id'];
        } else {
            $data['zone_id'] = $this->config->get('config_zone_id');
        }

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();




        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cart'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('checkout/checkout', '', true)
        );

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        $this->response->setOutput($this->load->view('checkout/checkout', $data));
    }

    public function country() {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id'        => $country_info['country_id'],
                'name'              => $country_info['name'],
                'iso_code_2'        => $country_info['iso_code_2'],
                'iso_code_3'        => $country_info['iso_code_3'],
                'address_format'    => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status'            => $country_info['status']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function customfield() {
        $json = array();

        $this->load->model('account/custom_field');

        // Customer Group
        if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->get['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required'        => $custom_field['required']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function validate() {
        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (empty($this->request->post['phone'])) {
            $this->error['phone'] = $this->language->get('error_phone');
        }

        if (empty($this->request->post['phone'])) {
            $this->error['phone'] = $this->language->get('error_phone');
        }

        if (isset($this->request->post['comment']) && (utf8_strlen($this->request->post['comment']) > 150)) {
            $this->error['comment'] = $this->language->get('error_comment');
        }

        if (isset($this->request->post['shipping']) && $this->request->post['shipping'] == 'delivery') {
            if (empty($this->request->post['address'])) {
                $this->error['address'] = $this->language->get('error_address');
            }

            if (empty($this->request->post['city'])) {
                $this->error['city'] = $this->language->get('error_city');
            }

            if (empty($this->request->post['code'])) {
                $this->error['code'] = $this->language->get('error_code');
            }

            if (!isset($this->request->post['country_id'])) {
                $this->error['country_id'] = $this->language->get('error_country_id');
            }

            if (!isset($this->request->post['zone_id'])) {
                $this->error['zone_id'] = $this->language->get('error_zone_id');
            }
        }

        if ($this->config->get('config_checkout_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if ($information_info && !isset($this->request->post['agree'])) {
                $this->error['agree'] = $this->language->get('error_agree');
            }
        }

        if (defined('reCAPTCHA_sitekey')) {
            if (isset($this->request->post['g-recaptcha-response'])) {
                # Verify captcha
                $post_data = http_build_query(array(
                    'secret'   => reCAPTCHA_secret,
                    'response' => $this->request->post['g-recaptcha-response'],
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                ));

                $opts = array('http' => array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $post_data
                ));

                $context  = stream_context_create($opts);
                $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
                $result = json_decode($response);
            }

            if (!isset($this->request->post['g-recaptcha-response']) || !$result->success) {
                $this->error['captcha'] = $this->language->get('error_captcha');
            }
        }

        if ($this->error) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    private function sendEmails(array $data = []) {
        if (!$data) {
            return false;
        }

        $this->load->language('checkout/checkout');

        $data['text_price'] = $this->language->get('text_price');
        $data['text_subject'] = $this->language->get('email_subject_admins');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_product'] = $this->language->get('text_product');
        $data['text_service'] = $this->language->get('text_service');
        $data['text_total'] = $this->language->get('text_total');
        $data['text_payment'] = $this->language->get('text_payment');
        $data['text_shipping'] = $this->language->get('text_shipping');

        $data['text_name'] = $this->language->get('text_name');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_phone'] = $this->language->get('text_phone');
        $data['text_comment'] = $this->language->get('text_comment');

        if ($data['shipping'] == 'delivery') {
            $data['text_address'] = $this->language->get('text_address');
            $data['text_city'] = $this->language->get('text_city');
            $data['text_code'] = $this->language->get('text_code');

            $this->load->model('localisation/country');
            $country_info = $this->model_localisation_country->getCountry($data['country_id']);
            if ($country_info) {
                $data['text_country'] = $this->language->get('text_country');
                $data['country'] = $country_info['name'];
            }

            $this->load->model('localisation/zone');
            $zone_info = $this->model_localisation_zone->getZone($data['zone_id']);
            if ($zone_info) {
                $data['text_zone'] = $this->language->get('text_zone');
                $data['zone'] = $zone_info['name'];
            }
        }

        $data['total'] = $this->cart->getTotal();
        $data['date_added'] = date('d.m.Y H:i');
        $data['products'] = $this->cart->getProducts();
        // $data['services'] = $this->cart->getServices();
        $data['payment'] = $this->language->get('text_payment_' . $data['payment']);
        $data['shipping2'] = $this->language->get('text_shipping_' . $data['shipping']);

        $mail = new Mail($this->config->get('config_mail_engine'));
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_mail_smtp_username'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode($this->language->get('email_subject_admins'), ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($this->load->view('mail/checkout_admins', $data));

        try {
            $mail->send();
        } catch (Exception $e) {
            $this->log->write('SMTP error in ' . __DIR__ . ' on line ' . __LINE__ . ': ',  $e->getMessage() . PHP_EOL);
        }

        // Send to customer email
        $email = $data['email'];

        if ($email && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $data['text_subject'] = $this->language->get('email_subject_customer');

            $mail->setTo($email);
            $mail->setFrom($this->config->get('config_mail_smtp_username'));
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($this->language->get('email_subject_customer'), ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($this->load->view('mail/checkout_customer', $data));

            try {
                $mail->send();
            } catch (Exception $e) {
                $this->log->write('SMTP error in ' . __DIR__ . ' on line ' . __LINE__ . ': ',  $e->getMessage() . PHP_EOL);
            }
        }

        // // Companies
        // $this->getCompanyEmails('product');
        // $this->getCompanyEmails('service');

        // // Send to company emails
        // foreach ($this->emails as $key => $company) {
        // 	if ($company['email'] && filter_var($company['email'], FILTER_VALIDATE_EMAIL)) {

        // 		$data['text_subject'] = $this->language->get('email_subject_admins');
        // 		$data['products'] = $company['products'];
        // 		$data['services'] = $company['services'];
        // 		$data['total'] = 0;

        // 		foreach ($company['products'] as $product) {
        // 			$data['total'] = $data['total'] + $product['total'];
        // 		}
        // 		foreach ($company['services'] as $service) {
        // 			$data['total'] = $data['total'] + $service['total'];
        // 		}

        // 		$mail->setTo($company['email']);
        // 		$mail->setFrom($this->config->get('config_mail_smtp_username'));
        // 		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        // 		$mail->setSubject(html_entity_decode($this->language->get('email_subject_admins'), ENT_QUOTES, 'UTF-8'));
        // 		$mail->setHtml($this->load->view('mail/checkout_admins', $data));

        // 		try {
        // 			$mail->send();
        // 		} catch (Exception $e) {
        // 			$this->log->write('SMTP error in ' . __DIR__ . ' on line ' . __LINE__ . ': ',  $e->getMessage() . PHP_EOL);
        // 		}
        // 	}
        // }

        return true;
    }

    private function getCompanyEmails($entity = 'product') {
        $this->load->model('account/customer');

        if ($entity == 'product') {
            $this->load->model('catalog/product');

            $products = $this->cart->getProducts();

            foreach ($products as $product) {
                $companies = $this->model_catalog_product->getCompanies($product['product_id']);

                foreach ($companies as $company) {
                    $this->pushEmail($company['company_id'], $product, 'product');
                }
            }
        } else {
            $this->load->model('service/service');

            $services = $this->cart->getServices();

            foreach ($services as $service) {
                $companies = $this->model_service_service->getCompanies($service['service_id']);

                foreach ($companies as $company) {
                    $this->pushEmail($company['company_id'], $service, 'service');
                }
            }
        }
    }

    private function pushEmail($company_id = 0, $entity = [], $type = 'product') {
        if ($company_id && $entity) {

            $customer_company = $this->model_account_customer->getCustomerByCompany($company_id);

            if ($customer_company) {
                $customer_info = $this->model_account_customer->getCustomer($customer_company['customer_id']);

                if ($customer_info) {
                    $this->emails[$company_id]['email'] = $customer_info['email'];

                    if (!isset($this->emails[$company_id]['products'])) {
                        $this->emails[$company_id]['products'] = [];
                    }

                    if (!isset($this->emails[$company_id]['services'])) {
                        $this->emails[$company_id]['services'] = [];
                    }

                    if ($type == 'product') {
                        $this->emails[$company_id]['products'][] = $entity;
                    }
                    if ($type == 'service') {
                        $this->emails[$company_id]['services'][] = $entity;
                    }
                }
            }
        }
    }
}