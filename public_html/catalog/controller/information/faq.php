<?php
class ControllerInformationFaq extends Controller {
    private $error = array();

    public function index() {
        $infoblocks = $this->load->controller('infoblock/infoblock');
        $categories = $this->load->controller('product/categories');
        $this->document->setTitle($infoblocks['seofaq']['infoblock_seofaq_title']);
        $this->document->setDescription($infoblocks['seofaq']['infoblock_seofaq_description']);
        $this->document->setKeywords($infoblocks['seofaq']['infoblock_seofaq_keyword']);
        $data['infoblocks'] = $infoblocks;
        $data['categories'] = $categories;
        $data['theme_dir'] = $this->config->get('theme_default_directory');

        $data['search_breadcrumb'] = $this->load->controller('common/search/breadcrumb');
        $this->load->language('information/faq');

        $data['heading_title'] = $this->language->get('text_faq');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $data['heading_title'],
            'href' => $this->url->link('information/faq')
        );

        // homefaq
        $this->load->model('infoblock/homefaq');
        $data['homefaqs'] = array();

        $results = $this->model_infoblock_homefaq->getHomefaqs();

        foreach ($results as $result) {
            $data['homefaqs'][] = array(
                'name' => $result['name'],
                'question' => $result['question'],
                'answer' => html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8')
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);
        $data['header'] = $this->load->controller('common/header', ['infoblocks' => $data['infoblocks'], 'categories' => $data['categories']]);

        $this->response->setOutput($this->load->view('information/faq', $data));
    }
}
