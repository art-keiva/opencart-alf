<?php
class ControllerInfoblockHomebanner5 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner5');

        $this->load->model('infoblock/homebanner5');

        $this->load->model('tool/image');

        $data['homebanner5s'] = [];

        $homebanner5s = $this->model_infoblock_homebanner5->getHomebanner5s();

        foreach ($homebanner5s as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 614, 830);
                $thumb = $this->model_tool_image->resize($result['image'], 240, 324);
            } else {
                $image = '';
                $thumb = '';
            }

            $data['homebanner5s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'thumb' => $thumb,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner5';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner5', $data);
    }
}
