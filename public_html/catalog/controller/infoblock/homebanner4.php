<?php
class ControllerInfoblockHomebanner4 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner4');

        $this->load->model('infoblock/homebanner4');

        $this->load->model('tool/image');

        $data['homebanner4s'] = [];

        $homebanner4s = $this->model_infoblock_homebanner4->getHomebanner4s();

        foreach ($homebanner4s as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 100, 100);
            } else {
                $image = '';
            }

            $data['homebanner4s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner4';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner4', $data);
    }
}
