<?php
class ControllerInfoblockHomebanner2 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner2');

        $this->load->model('infoblock/homebanner2');

        $this->load->model('tool/image');

        $data['homebanner2s'] = [];

        $homebanner2s = $this->model_infoblock_homebanner2->getHomebanner2s();

        foreach ($homebanner2s as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 100, 100);
            } else {
                $image = '';
            }

            $data['homebanner2s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner2';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner2', $data);
    }
}
