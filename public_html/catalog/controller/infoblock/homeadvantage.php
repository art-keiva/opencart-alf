<?php
class ControllerInfoblockHomeadvantage extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homeadvantage');

        $this->load->model('infoblock/homeadvantage');

        $this->load->model('tool/image');

        $data['homeadvantages'] = [];

        $homeadvantages = $this->model_infoblock_homeadvantage->getHomeadvantages();

        foreach ($homeadvantages as $result) {
            if ($result['image']) {
                // $image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 50, 60, 'max');
            } else {
                $image = '';
            }

            $data['homeadvantages'][] = [
                'name' => $result['name'],
                'description' => $result['description'],
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'advantage';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homeadvantage', $data);
    }
}