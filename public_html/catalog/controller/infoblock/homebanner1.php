<?php
class ControllerInfoblockHomebanner1 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner1');

        $this->load->model('infoblock/homebanner1');

        $this->load->model('tool/image');

        $data['homebanner1s'] = [];

        $homebanner1s = $this->model_infoblock_homebanner1->getHomebanner1s();

        foreach ($homebanner1s as $result) {
            if ($result['image']) {
                $image = HTTP_IMAGE . $result['image'];
                //$image = $this->model_tool_image->resize($result['image'], 50, 60, 'max');
            } else {
                $image = '';
            }

            $data['homebanner1s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner1';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner1', $data);
    }
}