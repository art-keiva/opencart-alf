<?php
class ControllerInfoblockHometestimonial extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/hometestimonial');

        $this->load->model('infoblock/hometestimonial');

        $this->load->model('tool/image');

        $data['hometestimonials'] = [];

        $hometestimonials = $this->model_infoblock_hometestimonial->getHometestimonials();

        foreach ($hometestimonials as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 92, 92);
            } else {
                $image = '';
            }

            $data['hometestimonials'][] = [
                'name' => $result['name'],
                'description' => html_entity_decode($result['description']),
                'job' => $result['job'],
                'image' => $image,
            ];
        }

        $tag = 'testimonial';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/hometestimonial', $data);
    }
}