<?php
class ControllerInfoblockHomefaq extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homefaq');

        $this->load->model('infoblock/homefaq');

        $data['homefaqs'] = [];

        $homefaqs = $this->model_infoblock_homefaq->getMainPageHomefaqs();

        foreach ($homefaqs as $result) {
            $data['homefaqs'][] = [
                'name' => $result['name'],
                'question' => $result['question'],
                'answer' => html_entity_decode($result['answer']),
            ];
        }

        $tag = 'faq';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homefaq', $data);
    }
}