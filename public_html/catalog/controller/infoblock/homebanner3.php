<?php
class ControllerInfoblockHomebanner3 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner3');

        $this->load->model('infoblock/homebanner3');

        $this->load->model('tool/image');

        $data['homebanner3s'] = [];

        $homebanner3s = $this->model_infoblock_homebanner3->getHomebanner3s();

        foreach ($homebanner3s as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 100, 100);
            } else {
                $image = '';
            }

            $data['homebanner3s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner3';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner3', $data);
    }
}
