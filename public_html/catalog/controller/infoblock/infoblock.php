<?php
class ControllerInfoblockInfoblock extends Controller {
    public function index() {
        $this->load->model('infoblock/infoblock');

        $infoblocks = $this->model_infoblock_infoblock->getInfoblocksSplittedByTag();

        return $infoblocks;
    }

    public function handleInfoblocks(array $infoblocks = [], string $type = 'contact') {
        $data = [];

        $i = 1;
        foreach ($infoblocks['infoblocks'] as $keyword => $value) {
            if ($keyword === $type) {
                foreach ($value as $key => $val) {
                    $data[$key] = $val;
                }
            }
            $i++;
        }

        return $data;
    }

    public function handleInfo(array $infoblocks = []) {
        $data = [];

        $i = 1;
        foreach ($infoblocks['infoblocks'] as $keyword => $value) {
            if ('info' . $i === $keyword) {
                foreach ($value as $key => $val) {
                    $parts = explode('_', $key);

                    if ($parts[2] === 'image') {
                        if ($val) {
                            $val = $this->model_tool_image->resize($val, 1920, 500);
                        } else {
                            $val = '';
                        }
                    }

                    $data[$key] = $val;
                }
            }
            $i++;
        }

        return $data;
    }
}