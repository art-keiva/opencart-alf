<?php
class ControllerInfoblockHomebanner6 extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homebanner6');

        $this->load->model('infoblock/homebanner6');

        $this->load->model('tool/image');

        $data['homebanner6s'] = [];

        $homebanner6s = $this->model_infoblock_homebanner6->getHomebanner6s();

        foreach ($homebanner6s as $result) {
            if ($result['image']) {
                //$image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 100, 100);
            } else {
                $image = '';
            }

            $data['homebanner6s'][] = [
                'name' => $result['name'],
                //'description' => $result['description'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'href' => $result['link'],
            ];
        }

        $tag = 'banner6';
        if (isset($parameters['infoblocks'][$tag])) {
            foreach ($parameters['infoblocks'][$tag] as $key => $value) {
                $data[$key] = $value;
            }
        }

        return $this->load->view('infoblock/homebanner6', $data);
    }
}
