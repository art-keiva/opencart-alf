<?php
class ControllerInfoblockHomeslider extends Controller {
    public function index($parameters = []) {
        $this->load->language('infoblock/homeslider');

        $this->load->model('infoblock/homeslider');

        $this->load->model('tool/image');

        $data['homesliders'] = [];

        $homesliders = $this->model_infoblock_homeslider->getHomesliders();

        foreach ($homesliders as $result) {
            if ($result['image']) {
                // $image = HTTP_IMAGE . $result['image'];
                $image = $this->model_tool_image->resize($result['image'], 1920, 500, 'max');
            } else {
                $image = '';
            }

            if ($result['image2']) {
                // $image2 = HTTP_IMAGE . $result['image2'];
                $image2 = $this->model_tool_image->resize($result['image2'], 1920, 500,'max');
            } else {
                $image2 = '';
            }

            $data['homesliders'][] = [
                'name' => $result['name'],
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'image' => $image,
                'image2' => $image2,
                'href' => $result['link'],
            ];
        }

        return $this->load->view('infoblock/homeslider', $data);
    }
}