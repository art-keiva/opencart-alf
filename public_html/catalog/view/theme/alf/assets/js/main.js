'use strict';

$(document).ready(function(){

	dropdownHoverShow();
	// checkCart();
	// goToContent();

	setTimeout(function () {
		autoHeight('.product-inner .caption h4');
	}, 100);



   // autoHeight(".tab-content .product-card__name");
   // autoHeight(".featureds .product-card__name");


});


function goToContent() {
	// catalog - drop to content area
	if ($('#content').length) {
		var currNavUrl = '';
		var currNavUrls = [];
		var splitPath = window.location.pathname.split('/');
		$(splitPath).each(function(index, value) {
			if (value.length && value == 'shop') {
				document.getElementById('content').scrollIntoView({
					behavior: 'smooth'
				});
				// currNavUrl += '/' + value;
				// currNavUrls.push(window.location.origin + currNavUrl);
			}
		});
	}
}

// Captcha
$(document).delegate('.captcha', 'click', function() {
	var date = new Date();
	$(this).prev().prop('src', 'index.php?route=extension/captcha/basic/captcha&' + date.getTime());
});

function dropdownHoverShow() {
	$('.main-menu .nav > .dropdown').on({
		mouseenter: function () {
			$(this).addClass('show');
			$(this).children('.dropdown-toggle').attr('aria-expanded', 'true');
			$(this).children('.dropdown-menu').addClass('show');
		},
		mouseleave: function () {
			$(this).removeClass('show');
			$(this).children('.dropdown-toggle').attr('aria-expanded', 'false');
			$(this).children('.dropdown-menu').removeClass('show');
		}
	});
}

function checkCart() {
	$.ajax({
		url: 'index.php?route=checkout/cart/check',
		type: 'post',
		data: 'is_empty=' + $('#empty-cart').length,
		dataType: 'json',
		beforeSend: function() {
			$('#cart > button').button('loading');
		},
		complete: function() {
			$('#cart > button').button('reset');
		},
		success: function(json) {

			if (json['total']) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		}
	});
}







function autoHeight(items_class)
{
    var maxHeight = 0;

    $(items_class).each(function(){
      if ( $(this).height() > maxHeight )
      {
        maxHeight = $(this).height();
      }
    });
     
    $(items_class).height(maxHeight);
}

